pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package libpng16_pnglibconf_h is

   PNG_API_RULE : constant := 0;  --  /opt/homebrew/include/libpng16/pnglibconf.h:197
   PNG_DEFAULT_READ_MACROS : constant := 1;  --  /opt/homebrew/include/libpng16/pnglibconf.h:198
   PNG_GAMMA_THRESHOLD_FIXED : constant := 5000;  --  /opt/homebrew/include/libpng16/pnglibconf.h:199
   --  unsupported macro: PNG_IDAT_READ_SIZE PNG_ZBUF_SIZE

   PNG_INFLATE_BUF_SIZE : constant := 1024;  --  /opt/homebrew/include/libpng16/pnglibconf.h:201
   --  unsupported macro: PNG_LINKAGE_API extern
   --  unsupported macro: PNG_LINKAGE_CALLBACK extern
   --  unsupported macro: PNG_LINKAGE_DATA extern
   --  unsupported macro: PNG_LINKAGE_FUNCTION extern

   PNG_MAX_GAMMA_8 : constant := 11;  --  /opt/homebrew/include/libpng16/pnglibconf.h:206
   PNG_QUANTIZE_BLUE_BITS : constant := 5;  --  /opt/homebrew/include/libpng16/pnglibconf.h:207
   PNG_QUANTIZE_GREEN_BITS : constant := 5;  --  /opt/homebrew/include/libpng16/pnglibconf.h:208
   PNG_QUANTIZE_RED_BITS : constant := 5;  --  /opt/homebrew/include/libpng16/pnglibconf.h:209
   PNG_TEXT_Z_DEFAULT_COMPRESSION : constant := (-1);  --  /opt/homebrew/include/libpng16/pnglibconf.h:210
   PNG_TEXT_Z_DEFAULT_STRATEGY : constant := 0;  --  /opt/homebrew/include/libpng16/pnglibconf.h:211
   PNG_USER_CHUNK_CACHE_MAX : constant := 1000;  --  /opt/homebrew/include/libpng16/pnglibconf.h:212
   PNG_USER_CHUNK_MALLOC_MAX : constant := 8000000;  --  /opt/homebrew/include/libpng16/pnglibconf.h:213
   PNG_USER_HEIGHT_MAX : constant := 1000000;  --  /opt/homebrew/include/libpng16/pnglibconf.h:214
   PNG_USER_WIDTH_MAX : constant := 1000000;  --  /opt/homebrew/include/libpng16/pnglibconf.h:215
   PNG_ZBUF_SIZE : constant := 8192;  --  /opt/homebrew/include/libpng16/pnglibconf.h:216
   PNG_ZLIB_VERNUM : constant := 16#12b0#;  --  /opt/homebrew/include/libpng16/pnglibconf.h:217
   PNG_Z_DEFAULT_COMPRESSION : constant := (-1);  --  /opt/homebrew/include/libpng16/pnglibconf.h:218
   PNG_Z_DEFAULT_NOFILTER_STRATEGY : constant := 0;  --  /opt/homebrew/include/libpng16/pnglibconf.h:219
   PNG_Z_DEFAULT_STRATEGY : constant := 1;  --  /opt/homebrew/include/libpng16/pnglibconf.h:220
   PNG_sCAL_PRECISION : constant := 5;  --  /opt/homebrew/include/libpng16/pnglibconf.h:221
   PNG_sRGB_PROFILE_CHECKS : constant := 2;  --  /opt/homebrew/include/libpng16/pnglibconf.h:222

end libpng16_pnglibconf_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
