pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package setjmp_h is

   type jmp_buf is array (0 .. 36) of aliased int;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/setjmp.h:37

   type sigjmp_buf is array (0 .. 37) of aliased int;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/setjmp.h:38

   function setjmp (arg1 : access int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/setjmp.h:87
   with Import => True, 
        Convention => C, 
        External_Name => "setjmp";

   procedure longjmp (arg1 : access int; arg2 : int)  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/setjmp.h:88
   with Import => True, 
        Convention => C, 
        External_Name => "longjmp";

   --  skipped func _setjmp

   --  skipped func _longjmp

   function sigsetjmp (arg1 : access int; arg2 : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/setjmp.h:93
   with Import => True, 
        Convention => C, 
        External_Name => "sigsetjmp";

   procedure siglongjmp (arg1 : access int; arg2 : int)  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/setjmp.h:94
   with Import => True, 
        Convention => C, 
        External_Name => "siglongjmp";

   procedure longjmperror  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/setjmp.h:98
   with Import => True, 
        Convention => C, 
        External_Name => "longjmperror";

end setjmp_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
