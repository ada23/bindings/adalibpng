pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with System;

package stddef_h is

   --  unsupported macro: NULL __null
   --  arg-macro: procedure offsetof (TYPE, MEMBER)
   --    __builtin_offsetof (TYPE, MEMBER)
   subtype ptrdiff_t is long;  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include/stddef.h:145

   subtype size_t is unsigned_long;  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include/stddef.h:214

   type max_align_t is record
      uu_max_align_ll : aliased Long_Long_Integer;  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include/stddef.h:426
      uu_max_align_ld : aliased long_double;  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include/stddef.h:427
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include/stddef.h:436

   subtype nullptr_t is System.Address;  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include/stddef.h:443

end stddef_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
