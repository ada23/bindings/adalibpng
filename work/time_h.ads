pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Strings;
with sys_utypes_uclock_t_h;
with sys_utypes_utime_t_h;
with stddef_h;
limited with sys_utypes_utimespec_h;
with i386_utypes_h;

package time_h is

   --  unsupported macro: CLOCKS_PER_SEC ((clock_t)1000000)
   --  unsupported macro: CLOCK_REALTIME _CLOCK_REALTIME
   --  unsupported macro: CLOCK_MONOTONIC _CLOCK_MONOTONIC
   --  unsupported macro: CLOCK_MONOTONIC_RAW _CLOCK_MONOTONIC_RAW
   --  unsupported macro: CLOCK_MONOTONIC_RAW_APPROX _CLOCK_MONOTONIC_RAW_APPROX
   --  unsupported macro: CLOCK_UPTIME_RAW _CLOCK_UPTIME_RAW
   --  unsupported macro: CLOCK_UPTIME_RAW_APPROX _CLOCK_UPTIME_RAW_APPROX
   --  unsupported macro: CLOCK_PROCESS_CPUTIME_ID _CLOCK_PROCESS_CPUTIME_ID
   --  unsupported macro: CLOCK_THREAD_CPUTIME_ID _CLOCK_THREAD_CPUTIME_ID
   TIME_UTC : constant := 1;  --  /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:197

   type tm is record
      tm_sec : aliased int;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:76
      tm_min : aliased int;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:77
      tm_hour : aliased int;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:78
      tm_mday : aliased int;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:79
      tm_mon : aliased int;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:80
      tm_year : aliased int;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:81
      tm_wday : aliased int;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:82
      tm_yday : aliased int;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:83
      tm_isdst : aliased int;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:84
      tm_gmtoff : aliased long;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:85
      tm_zone : Interfaces.C.Strings.chars_ptr;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:86
   end record
   with Convention => C_Pass_By_Copy;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:75

   tzname : array (size_t) of Interfaces.C.Strings.chars_ptr  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:98
   with Import => True, 
        Convention => C, 
        External_Name => "tzname";

   getdate_err : aliased int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:101
   with Import => True, 
        Convention => C, 
        External_Name => "getdate_err";

   timezone : aliased long  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:103
   with Import => True, 
        Convention => C, 
        External_Name => "_timezone";

   daylight : aliased int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:105
   with Import => True, 
        Convention => C, 
        External_Name => "daylight";

   function asctime (arg1 : access constant tm) return Interfaces.C.Strings.chars_ptr  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:108
   with Import => True, 
        Convention => C, 
        External_Name => "asctime";

   function clock return sys_utypes_uclock_t_h.clock_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:109
   with Import => True, 
        Convention => C, 
        External_Name => "_clock";

   function ctime (arg1 : access sys_utypes_utime_t_h.time_t) return Interfaces.C.Strings.chars_ptr  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:110
   with Import => True, 
        Convention => C, 
        External_Name => "ctime";

   function difftime (arg1 : sys_utypes_utime_t_h.time_t; arg2 : sys_utypes_utime_t_h.time_t) return double  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:111
   with Import => True, 
        Convention => C, 
        External_Name => "difftime";

   function getdate (arg1 : Interfaces.C.Strings.chars_ptr) return access tm  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:112
   with Import => True, 
        Convention => C, 
        External_Name => "getdate";

   function gmtime (arg1 : access sys_utypes_utime_t_h.time_t) return access tm  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:113
   with Import => True, 
        Convention => C, 
        External_Name => "gmtime";

   function localtime (arg1 : access sys_utypes_utime_t_h.time_t) return access tm  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:114
   with Import => True, 
        Convention => C, 
        External_Name => "localtime";

   function mktime (arg1 : access tm) return sys_utypes_utime_t_h.time_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:115
   with Import => True, 
        Convention => C, 
        External_Name => "_mktime";

   function strftime
     (arg1 : Interfaces.C.Strings.chars_ptr;
      arg2 : stddef_h.size_t;
      arg3 : Interfaces.C.Strings.chars_ptr;
      arg4 : access constant tm) return stddef_h.size_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:116
   with Import => True, 
        Convention => C, 
        External_Name => "_strftime";

   function strptime
     (arg1 : Interfaces.C.Strings.chars_ptr;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : access tm) return Interfaces.C.Strings.chars_ptr  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:117
   with Import => True, 
        Convention => C, 
        External_Name => "_strptime";

   function time (arg1 : access sys_utypes_utime_t_h.time_t) return sys_utypes_utime_t_h.time_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:118
   with Import => True, 
        Convention => C, 
        External_Name => "time";

   procedure tzset  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:121
   with Import => True, 
        Convention => C, 
        External_Name => "tzset";

   function asctime_r (arg1 : access constant tm; arg2 : Interfaces.C.Strings.chars_ptr) return Interfaces.C.Strings.chars_ptr  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:125
   with Import => True, 
        Convention => C, 
        External_Name => "asctime_r";

   function ctime_r (arg1 : access sys_utypes_utime_t_h.time_t; arg2 : Interfaces.C.Strings.chars_ptr) return Interfaces.C.Strings.chars_ptr  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:126
   with Import => True, 
        Convention => C, 
        External_Name => "ctime_r";

   function gmtime_r (arg1 : access sys_utypes_utime_t_h.time_t; arg2 : access tm) return access tm  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:127
   with Import => True, 
        Convention => C, 
        External_Name => "gmtime_r";

   function localtime_r (arg1 : access sys_utypes_utime_t_h.time_t; arg2 : access tm) return access tm  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:128
   with Import => True, 
        Convention => C, 
        External_Name => "localtime_r";

   function posix2time (arg1 : sys_utypes_utime_t_h.time_t) return sys_utypes_utime_t_h.time_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:131
   with Import => True, 
        Convention => C, 
        External_Name => "posix2time";

   procedure tzsetwall  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:135
   with Import => True, 
        Convention => C, 
        External_Name => "tzsetwall";

   function time2posix (arg1 : sys_utypes_utime_t_h.time_t) return sys_utypes_utime_t_h.time_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:136
   with Import => True, 
        Convention => C, 
        External_Name => "time2posix";

   function timelocal (arg1 : access tm) return sys_utypes_utime_t_h.time_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:137
   with Import => True, 
        Convention => C, 
        External_Name => "timelocal";

   function timegm (arg1 : access tm) return sys_utypes_utime_t_h.time_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:138
   with Import => True, 
        Convention => C, 
        External_Name => "timegm";

   function nanosleep (uu_rqtp : access constant sys_utypes_utimespec_h.timespec; uu_rmtp : access sys_utypes_utimespec_h.timespec) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:142
   with Import => True, 
        Convention => C, 
        External_Name => "_nanosleep";

   subtype clockid_t is unsigned;
   clockid_t_u_CLOCK_REALTIME : constant clockid_t := 0;
   clockid_t_u_CLOCK_MONOTONIC : constant clockid_t := 6;
   clockid_t_u_CLOCK_MONOTONIC_RAW : constant clockid_t := 4;
   clockid_t_u_CLOCK_MONOTONIC_RAW_APPROX : constant clockid_t := 5;
   clockid_t_u_CLOCK_UPTIME_RAW : constant clockid_t := 8;
   clockid_t_u_CLOCK_UPTIME_RAW_APPROX : constant clockid_t := 9;
   clockid_t_u_CLOCK_PROCESS_CPUTIME_ID : constant clockid_t := 12;
   clockid_t_u_CLOCK_THREAD_CPUTIME_ID : constant clockid_t := 16;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:172

   function clock_getres (uu_clock_id : clockid_t; uu_res : access sys_utypes_utimespec_h.timespec) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:175
   with Import => True, 
        Convention => C, 
        External_Name => "clock_getres";

   function clock_gettime (uu_clock_id : clockid_t; uu_tp : access sys_utypes_utimespec_h.timespec) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:178
   with Import => True, 
        Convention => C, 
        External_Name => "clock_gettime";

   function clock_gettime_nsec_np (uu_clock_id : clockid_t) return i386_utypes_h.uu_uint64_t  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:182
   with Import => True, 
        Convention => C, 
        External_Name => "clock_gettime_nsec_np";

   function clock_settime (uu_clock_id : clockid_t; uu_tp : access constant sys_utypes_utimespec_h.timespec) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:187
   with Import => True, 
        Convention => C, 
        External_Name => "clock_settime";

   function timespec_get (ts : access sys_utypes_utimespec_h.timespec; base : int) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/time.h:199
   with Import => True, 
        Convention => C, 
        External_Name => "timespec_get";

end time_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
