pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with ustdio_h;
with Interfaces.C.Strings;
with System;
with stddef_h;
with sys_utypes_uoff_t_h;
with sys_utypes_ussize_t_h;

package stdio_h is

   FIXINC_WRAP_STDIO_H_STDIO_STDARG_H : constant := 1;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:11

   BUFSIZ : constant := 1024;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:117
   EOF : constant := (-1);  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:118

   FOPEN_MAX : constant := 20;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:121
   FILENAME_MAX : constant := 1024;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:122

   P_tmpdir : aliased constant String := "/var/tmp/" & ASCII.NUL;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:126

   L_tmpnam : constant := 1024;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:128
   TMP_MAX : constant := 308915776;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:129

   SEEK_SET : constant := 0;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:132

   SEEK_CUR : constant := 1;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:135

   SEEK_END : constant := 2;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:138
   --  unsupported macro: stdin __stdinp
   --  unsupported macro: stdout __stdoutp
   --  unsupported macro: stderr __stderrp

   L_ctermid : constant := 1024;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:241
   --  arg-macro: procedure getc_unlocked (fp)
   --    __sgetc(fp)
   --  arg-macro: procedure putc_unlocked (x, fp)
   --    __sputc(x, fp)
   --  arg-macro: procedure getchar_unlocked ()
   --    getc_unlocked(stdin)
   --  arg-macro: procedure putchar_unlocked (x)
   --    putc_unlocked(x, stdout)
   --  arg-macro: procedure fropen (cookie, fn)
   --    funopen(cookie, fn, 0, 0, 0)
   --  arg-macro: procedure fwopen (cookie, fn)
   --    funopen(cookie, 0, fn, 0, 0)
   --  arg-macro: procedure feof_unlocked (p)
   --    __sfeof(p)
   --  arg-macro: procedure ferror_unlocked (p)
   --    __sferror(p)
   --  arg-macro: procedure clearerr_unlocked (p)
   --    __sclearerr(p)
   --  arg-macro: procedure fileno_unlocked (p)
   --    __sfileno(p)

   procedure clearerr (arg1 : access ustdio_h.uu_sFILE)  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:156
   with Import => True, 
        Convention => C, 
        External_Name => "clearerr";

   function fclose (arg1 : access ustdio_h.uu_sFILE) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:157
   with Import => True, 
        Convention => C, 
        External_Name => "fclose";

   function feof (arg1 : access ustdio_h.uu_sFILE) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:158
   with Import => True, 
        Convention => C, 
        External_Name => "feof";

   function ferror (arg1 : access ustdio_h.uu_sFILE) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:159
   with Import => True, 
        Convention => C, 
        External_Name => "ferror";

   function fflush (arg1 : access ustdio_h.uu_sFILE) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:160
   with Import => True, 
        Convention => C, 
        External_Name => "fflush";

   function fgetc (arg1 : access ustdio_h.uu_sFILE) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:161
   with Import => True, 
        Convention => C, 
        External_Name => "fgetc";

   function fgetpos (arg1 : access ustdio_h.uu_sFILE; arg2 : access ustdio_h.fpos_t) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:162
   with Import => True, 
        Convention => C, 
        External_Name => "fgetpos";

   function fgets
     (arg1 : Interfaces.C.Strings.chars_ptr;
      arg2 : int;
      arg3 : access ustdio_h.uu_sFILE) return Interfaces.C.Strings.chars_ptr  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:163
   with Import => True, 
        Convention => C, 
        External_Name => "fgets";

   function fopen (uu_filename : Interfaces.C.Strings.chars_ptr; uu_mode : Interfaces.C.Strings.chars_ptr) return access ustdio_h.uu_sFILE  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:167
   with Import => True, 
        Convention => C, 
        External_Name => "_fopen";

   function fprintf (arg1 : access ustdio_h.uu_sFILE; arg2 : Interfaces.C.Strings.chars_ptr  -- , ...
      ) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:169
   with Import => True, 
        Convention => C, 
        External_Name => "fprintf";

   function fputc (arg1 : int; arg2 : access ustdio_h.uu_sFILE) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:170
   with Import => True, 
        Convention => C, 
        External_Name => "fputc";

   function fputs (arg1 : Interfaces.C.Strings.chars_ptr; arg2 : access ustdio_h.uu_sFILE) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:171
   with Import => True, 
        Convention => C, 
        External_Name => "_fputs";

   function fread
     (uu_ptr : System.Address;
      uu_size : stddef_h.size_t;
      uu_nitems : stddef_h.size_t;
      uu_stream : access ustdio_h.uu_sFILE) return stddef_h.size_t  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:172
   with Import => True, 
        Convention => C, 
        External_Name => "fread";

   function freopen
     (arg1 : Interfaces.C.Strings.chars_ptr;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : access ustdio_h.uu_sFILE) return access ustdio_h.uu_sFILE  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:173
   with Import => True, 
        Convention => C, 
        External_Name => "_freopen";

   function fscanf (arg1 : access ustdio_h.uu_sFILE; arg2 : Interfaces.C.Strings.chars_ptr  -- , ...
      ) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:175
   with Import => True, 
        Convention => C, 
        External_Name => "fscanf";

   function fseek
     (arg1 : access ustdio_h.uu_sFILE;
      arg2 : long;
      arg3 : int) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:176
   with Import => True, 
        Convention => C, 
        External_Name => "fseek";

   function fsetpos (arg1 : access ustdio_h.uu_sFILE; arg2 : access ustdio_h.fpos_t) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:177
   with Import => True, 
        Convention => C, 
        External_Name => "fsetpos";

   function ftell (arg1 : access ustdio_h.uu_sFILE) return long  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:178
   with Import => True, 
        Convention => C, 
        External_Name => "ftell";

   function fwrite
     (uu_ptr : System.Address;
      uu_size : stddef_h.size_t;
      uu_nitems : stddef_h.size_t;
      uu_stream : access ustdio_h.uu_sFILE) return stddef_h.size_t  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:179
   with Import => True, 
        Convention => C, 
        External_Name => "_fwrite";

   function getc (arg1 : access ustdio_h.uu_sFILE) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:180
   with Import => True, 
        Convention => C, 
        External_Name => "getc";

   function getchar return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:181
   with Import => True, 
        Convention => C, 
        External_Name => "getchar";

   function gets (arg1 : Interfaces.C.Strings.chars_ptr) return Interfaces.C.Strings.chars_ptr  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:188
   with Import => True, 
        Convention => C, 
        External_Name => "gets";

   procedure perror (arg1 : Interfaces.C.Strings.chars_ptr)  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:190
   with Import => True, 
        Convention => C, 
        External_Name => "perror";

   function printf (arg1 : Interfaces.C.Strings.chars_ptr  -- , ...
      ) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:191
   with Import => True, 
        Convention => C, 
        External_Name => "printf";

   function putc (arg1 : int; arg2 : access ustdio_h.uu_sFILE) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:192
   with Import => True, 
        Convention => C, 
        External_Name => "putc";

   function putchar (arg1 : int) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:193
   with Import => True, 
        Convention => C, 
        External_Name => "putchar";

   function puts (arg1 : Interfaces.C.Strings.chars_ptr) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:194
   with Import => True, 
        Convention => C, 
        External_Name => "puts";

   function remove (arg1 : Interfaces.C.Strings.chars_ptr) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:195
   with Import => True, 
        Convention => C, 
        External_Name => "remove";

   function rename (uu_old : Interfaces.C.Strings.chars_ptr; uu_new : Interfaces.C.Strings.chars_ptr) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:196
   with Import => True, 
        Convention => C, 
        External_Name => "rename";

   procedure rewind (arg1 : access ustdio_h.uu_sFILE)  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:197
   with Import => True, 
        Convention => C, 
        External_Name => "rewind";

   function scanf (arg1 : Interfaces.C.Strings.chars_ptr  -- , ...
      ) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:198
   with Import => True, 
        Convention => C, 
        External_Name => "scanf";

   procedure setbuf (arg1 : access ustdio_h.uu_sFILE; arg2 : Interfaces.C.Strings.chars_ptr)  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:199
   with Import => True, 
        Convention => C, 
        External_Name => "setbuf";

   function setvbuf
     (arg1 : access ustdio_h.uu_sFILE;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : int;
      arg4 : stddef_h.size_t) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:200
   with Import => True, 
        Convention => C, 
        External_Name => "setvbuf";

   function sprintf (arg1 : Interfaces.C.Strings.chars_ptr; arg2 : Interfaces.C.Strings.chars_ptr  -- , ...
      ) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:208
   with Import => True, 
        Convention => C, 
        External_Name => "sprintf";

   function sscanf (arg1 : Interfaces.C.Strings.chars_ptr; arg2 : Interfaces.C.Strings.chars_ptr  -- , ...
      ) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:210
   with Import => True, 
        Convention => C, 
        External_Name => "sscanf";

   function tmpfile return access ustdio_h.uu_sFILE  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:211
   with Import => True, 
        Convention => C, 
        External_Name => "tmpfile";

   function tmpnam (arg1 : Interfaces.C.Strings.chars_ptr) return Interfaces.C.Strings.chars_ptr  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:219
   with Import => True, 
        Convention => C, 
        External_Name => "tmpnam";

   function ungetc (arg1 : int; arg2 : access ustdio_h.uu_sFILE) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:221
   with Import => True, 
        Convention => C, 
        External_Name => "ungetc";

   function vfprintf
     (arg1 : access ustdio_h.uu_sFILE;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : access System.Address) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:222
   with Import => True, 
        Convention => C, 
        External_Name => "vfprintf";

   function vprintf (arg1 : Interfaces.C.Strings.chars_ptr; arg2 : access System.Address) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:223
   with Import => True, 
        Convention => C, 
        External_Name => "vprintf";

   function vsprintf
     (arg1 : Interfaces.C.Strings.chars_ptr;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : access System.Address) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:231
   with Import => True, 
        Convention => C, 
        External_Name => "vsprintf";

   function fdopen (arg1 : int; arg2 : Interfaces.C.Strings.chars_ptr) return access ustdio_h.uu_sFILE  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:249
   with Import => True, 
        Convention => C, 
        External_Name => "_fdopen";

   function fileno (arg1 : access ustdio_h.uu_sFILE) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:251
   with Import => True, 
        Convention => C, 
        External_Name => "fileno";

   function pclose (arg1 : access ustdio_h.uu_sFILE) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:262
   with Import => True, 
        Convention => C, 
        External_Name => "pclose";

   function popen (arg1 : Interfaces.C.Strings.chars_ptr; arg2 : Interfaces.C.Strings.chars_ptr) return access ustdio_h.uu_sFILE  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:266
   with Import => True, 
        Convention => C, 
        External_Name => "_popen";

   --  skipped func __srget

   --  skipped func __svfscanf

   --  skipped func __swbuf

   --  skipped func __sputc

   procedure flockfile (arg1 : access ustdio_h.uu_sFILE)  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:318
   with Import => True, 
        Convention => C, 
        External_Name => "flockfile";

   function ftrylockfile (arg1 : access ustdio_h.uu_sFILE) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:319
   with Import => True, 
        Convention => C, 
        External_Name => "ftrylockfile";

   procedure funlockfile (arg1 : access ustdio_h.uu_sFILE)  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:320
   with Import => True, 
        Convention => C, 
        External_Name => "funlockfile";

   function getc_unlocked (arg1 : access ustdio_h.uu_sFILE) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:321
   with Import => True, 
        Convention => C, 
        External_Name => "getc_unlocked";

   function getchar_unlocked return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:322
   with Import => True, 
        Convention => C, 
        External_Name => "getchar_unlocked";

   function putc_unlocked (arg1 : int; arg2 : access ustdio_h.uu_sFILE) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:323
   with Import => True, 
        Convention => C, 
        External_Name => "putc_unlocked";

   function putchar_unlocked (arg1 : int) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:324
   with Import => True, 
        Convention => C, 
        External_Name => "putchar_unlocked";

   function getw (arg1 : access ustdio_h.uu_sFILE) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:328
   with Import => True, 
        Convention => C, 
        External_Name => "getw";

   function putw (arg1 : int; arg2 : access ustdio_h.uu_sFILE) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:329
   with Import => True, 
        Convention => C, 
        External_Name => "putw";

   function tempnam (uu_dir : Interfaces.C.Strings.chars_ptr; uu_prefix : Interfaces.C.Strings.chars_ptr) return Interfaces.C.Strings.chars_ptr  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:338
   with Import => True, 
        Convention => C, 
        External_Name => "_tempnam";

   function fseeko
     (uu_stream : access ustdio_h.uu_sFILE;
      uu_offset : sys_utypes_uoff_t_h.off_t;
      uu_whence : int) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:361
   with Import => True, 
        Convention => C, 
        External_Name => "fseeko";

   function ftello (uu_stream : access ustdio_h.uu_sFILE) return sys_utypes_uoff_t_h.off_t  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:362
   with Import => True, 
        Convention => C, 
        External_Name => "ftello";

   function snprintf
     (uu_str : Interfaces.C.Strings.chars_ptr;
      uu_size : stddef_h.size_t;
      uu_format : Interfaces.C.Strings.chars_ptr  -- , ...
      ) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:368
   with Import => True, 
        Convention => C, 
        External_Name => "snprintf";

   function vfscanf
     (uu_stream : access ustdio_h.uu_sFILE;
      uu_format : Interfaces.C.Strings.chars_ptr;
      arg3 : access System.Address) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:369
   with Import => True, 
        Convention => C, 
        External_Name => "vfscanf";

   function vscanf (uu_format : Interfaces.C.Strings.chars_ptr; arg2 : access System.Address) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:370
   with Import => True, 
        Convention => C, 
        External_Name => "vscanf";

   function vsnprintf
     (uu_str : Interfaces.C.Strings.chars_ptr;
      uu_size : stddef_h.size_t;
      uu_format : Interfaces.C.Strings.chars_ptr;
      arg4 : access System.Address) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:371
   with Import => True, 
        Convention => C, 
        External_Name => "vsnprintf";

   function vsscanf
     (uu_str : Interfaces.C.Strings.chars_ptr;
      uu_format : Interfaces.C.Strings.chars_ptr;
      arg3 : access System.Address) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:372
   with Import => True, 
        Convention => C, 
        External_Name => "vsscanf";

   function dprintf (arg1 : int; arg2 : Interfaces.C.Strings.chars_ptr  -- , ...
      ) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:386
   with Import => True, 
        Convention => C, 
        External_Name => "dprintf";

   function vdprintf
     (arg1 : int;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : access System.Address) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:387
   with Import => True, 
        Convention => C, 
        External_Name => "vdprintf";

   function getdelim
     (uu_linep : System.Address;
      uu_linecapp : access stddef_h.size_t;
      uu_delimiter : int;
      uu_stream : access ustdio_h.uu_sFILE) return sys_utypes_ussize_t_h.ssize_t  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:388
   with Import => True, 
        Convention => C, 
        External_Name => "getdelim";

   function getline
     (uu_linep : System.Address;
      uu_linecapp : access stddef_h.size_t;
      uu_stream : access ustdio_h.uu_sFILE) return sys_utypes_ussize_t_h.ssize_t  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:389
   with Import => True, 
        Convention => C, 
        External_Name => "getline";

   function fmemopen
     (uu_buf : System.Address;
      uu_size : stddef_h.size_t;
      uu_mode : Interfaces.C.Strings.chars_ptr) return access ustdio_h.uu_sFILE  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:390
   with Import => True, 
        Convention => C, 
        External_Name => "fmemopen";

   function open_memstream (uu_bufp : System.Address; uu_sizep : access stddef_h.size_t) return access ustdio_h.uu_sFILE  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:391
   with Import => True, 
        Convention => C, 
        External_Name => "open_memstream";

   sys_nerr : aliased constant int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:401
   with Import => True, 
        Convention => C, 
        External_Name => "sys_nerr";

   sys_errlist : array (size_t) of Interfaces.C.Strings.chars_ptr  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:402
   with Import => True, 
        Convention => C, 
        External_Name => "sys_errlist";

   function asprintf (arg1 : System.Address; arg2 : Interfaces.C.Strings.chars_ptr  -- , ...
      ) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:404
   with Import => True, 
        Convention => C, 
        External_Name => "asprintf";

   function ctermid_r (arg1 : Interfaces.C.Strings.chars_ptr) return Interfaces.C.Strings.chars_ptr  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:405
   with Import => True, 
        Convention => C, 
        External_Name => "ctermid_r";

   function fgetln (arg1 : access ustdio_h.uu_sFILE; arg2 : access stddef_h.size_t) return Interfaces.C.Strings.chars_ptr  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:406
   with Import => True, 
        Convention => C, 
        External_Name => "fgetln";

   function fmtcheck (arg1 : Interfaces.C.Strings.chars_ptr; arg2 : Interfaces.C.Strings.chars_ptr) return Interfaces.C.Strings.chars_ptr  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:407
   with Import => True, 
        Convention => C, 
        External_Name => "fmtcheck";

   function fpurge (arg1 : access ustdio_h.uu_sFILE) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:408
   with Import => True, 
        Convention => C, 
        External_Name => "fpurge";

   procedure setbuffer
     (arg1 : access ustdio_h.uu_sFILE;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : int)  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:409
   with Import => True, 
        Convention => C, 
        External_Name => "setbuffer";

   function setlinebuf (arg1 : access ustdio_h.uu_sFILE) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:410
   with Import => True, 
        Convention => C, 
        External_Name => "setlinebuf";

   function vasprintf
     (arg1 : System.Address;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : access System.Address) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:411
   with Import => True, 
        Convention => C, 
        External_Name => "vasprintf";

   function funopen
     (arg1 : System.Address;
      arg2 : access function
        (arg1 : System.Address;
         arg2 : Interfaces.C.Strings.chars_ptr;
         arg3 : int) return int;
      arg3 : access function
        (arg1 : System.Address;
         arg2 : Interfaces.C.Strings.chars_ptr;
         arg3 : int) return int;
      arg4 : access function
        (arg1 : System.Address;
         arg2 : ustdio_h.fpos_t;
         arg3 : int) return ustdio_h.fpos_t;
      arg5 : access function (arg1 : System.Address) return int) return access ustdio_h.uu_sFILE  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/stdio.h:417
   with Import => True, 
        Convention => C, 
        External_Name => "funopen";

end stdio_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
