pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Strings;
with System;
with adalibpng ;
with adalibpng.conf ; 


package png is

   PNG_LIBPNG_VER_STRING : aliased constant String := "1.6.43" & ASCII.NUL;  --  /opt/homebrew/include/libpng16/png.h:278
   --  unsupported macro: PNG_HEADER_VERSION_STRING " libpng version " PNG_LIBPNG_VER_STRING "\n"

   PNG_LIBPNG_VER_SHAREDLIB : constant := 16;  --  /opt/homebrew/include/libpng16/png.h:282
   --  unsupported macro: PNG_LIBPNG_VER_SONUM PNG_LIBPNG_VER_SHAREDLIB
   --  unsupported macro: PNG_LIBPNG_VER_DLLNUM PNG_LIBPNG_VER_SHAREDLIB

   PNG_LIBPNG_VER_MAJOR : constant := 1;  --  /opt/homebrew/include/libpng16/png.h:287
   PNG_LIBPNG_VER_MINOR : constant := 6;  --  /opt/homebrew/include/libpng16/png.h:288
   PNG_LIBPNG_VER_RELEASE : constant := 43;  --  /opt/homebrew/include/libpng16/png.h:289

   PNG_LIBPNG_VER_BUILD : constant := 0;  --  /opt/homebrew/include/libpng16/png.h:294

   PNG_LIBPNG_BUILD_ALPHA : constant := 1;  --  /opt/homebrew/include/libpng16/png.h:297
   PNG_LIBPNG_BUILD_BETA : constant := 2;  --  /opt/homebrew/include/libpng16/png.h:298
   PNG_LIBPNG_BUILD_RC : constant := 3;  --  /opt/homebrew/include/libpng16/png.h:299
   PNG_LIBPNG_BUILD_STABLE : constant := 4;  --  /opt/homebrew/include/libpng16/png.h:300
   PNG_LIBPNG_BUILD_RELEASE_STATUS_MASK : constant := 7;  --  /opt/homebrew/include/libpng16/png.h:301

   PNG_LIBPNG_BUILD_PATCH : constant := 8;  --  /opt/homebrew/include/libpng16/png.h:304

   PNG_LIBPNG_BUILD_PRIVATE : constant := 16;  --  /opt/homebrew/include/libpng16/png.h:306

   PNG_LIBPNG_BUILD_SPECIAL : constant := 32;  --  /opt/homebrew/include/libpng16/png.h:308
   --  unsupported macro: PNG_LIBPNG_BUILD_BASE_TYPE PNG_LIBPNG_BUILD_STABLE

   PNG_LIBPNG_VER : constant := 10643;  --  /opt/homebrew/include/libpng16/png.h:320
   --  unsupported macro: PNG_LIBPNG_BUILD_TYPE (PNG_LIBPNG_BUILD_BASE_TYPE)
   --  unsupported macro: png_libpng_ver png_get_header_ver(NULL)

   PNG_TEXT_COMPRESSION_NONE_WR : constant := -3;  --  /opt/homebrew/include/libpng16/png.h:585
   PNG_TEXT_COMPRESSION_zTXt_WR : constant := -2;  --  /opt/homebrew/include/libpng16/png.h:586
   PNG_TEXT_COMPRESSION_NONE : constant := -1;  --  /opt/homebrew/include/libpng16/png.h:587
   PNG_TEXT_COMPRESSION_zTXt : constant := 0;  --  /opt/homebrew/include/libpng16/png.h:588
   PNG_ITXT_COMPRESSION_NONE : constant := 1;  --  /opt/homebrew/include/libpng16/png.h:589
   PNG_ITXT_COMPRESSION_zTXt : constant := 2;  --  /opt/homebrew/include/libpng16/png.h:590
   PNG_TEXT_COMPRESSION_LAST : constant := 3;  --  /opt/homebrew/include/libpng16/png.h:591

   PNG_HAVE_IHDR : constant := 16#01#;  --  /opt/homebrew/include/libpng16/png.h:643
   PNG_HAVE_PLTE : constant := 16#02#;  --  /opt/homebrew/include/libpng16/png.h:644
   PNG_AFTER_IDAT : constant := 16#08#;  --  /opt/homebrew/include/libpng16/png.h:645
   --  unsupported macro: PNG_UINT_31_MAX ((png_uint_32)0x7fffffffL)
   --  unsupported macro: PNG_UINT_32_MAX ((png_uint_32)(-1))
   --  unsupported macro: PNG_SIZE_MAX ((size_t)(-1))

   PNG_FP_1 : constant := 100000;  --  /opt/homebrew/include/libpng16/png.h:655
   PNG_FP_HALF : constant := 50000;  --  /opt/homebrew/include/libpng16/png.h:656
   --  unsupported macro: PNG_FP_MAX ((png_fixed_point)0x7fffffffL)
   --  unsupported macro: PNG_FP_MIN (-PNG_FP_MAX)

   PNG_COLOR_MASK_PALETTE : constant := 1;  --  /opt/homebrew/include/libpng16/png.h:662
   PNG_COLOR_MASK_COLOR : constant := 2;  --  /opt/homebrew/include/libpng16/png.h:663
   PNG_COLOR_MASK_ALPHA : constant := 4;  --  /opt/homebrew/include/libpng16/png.h:664

   PNG_COLOR_TYPE_GRAY : constant := 0;  --  /opt/homebrew/include/libpng16/png.h:667
   --  unsupported macro: PNG_COLOR_TYPE_PALETTE (PNG_COLOR_MASK_COLOR | PNG_COLOR_MASK_PALETTE)
   --  unsupported macro: PNG_COLOR_TYPE_RGB (PNG_COLOR_MASK_COLOR)
   --  unsupported macro: PNG_COLOR_TYPE_RGB_ALPHA (PNG_COLOR_MASK_COLOR | PNG_COLOR_MASK_ALPHA)
   --  unsupported macro: PNG_COLOR_TYPE_GRAY_ALPHA (PNG_COLOR_MASK_ALPHA)
   --  unsupported macro: PNG_COLOR_TYPE_RGBA PNG_COLOR_TYPE_RGB_ALPHA
   --  unsupported macro: PNG_COLOR_TYPE_GA PNG_COLOR_TYPE_GRAY_ALPHA

   PNG_COMPRESSION_TYPE_BASE : constant := 0;  --  /opt/homebrew/include/libpng16/png.h:677
   --  unsupported macro: PNG_COMPRESSION_TYPE_DEFAULT PNG_COMPRESSION_TYPE_BASE

   PNG_FILTER_TYPE_BASE : constant := 0;  --  /opt/homebrew/include/libpng16/png.h:681
   PNG_INTRAPIXEL_DIFFERENCING : constant := 64;  --  /opt/homebrew/include/libpng16/png.h:682
   --  unsupported macro: PNG_FILTER_TYPE_DEFAULT PNG_FILTER_TYPE_BASE

   PNG_INTERLACE_NONE : constant := 0;  --  /opt/homebrew/include/libpng16/png.h:686
   PNG_INTERLACE_ADAM7 : constant := 1;  --  /opt/homebrew/include/libpng16/png.h:687
   PNG_INTERLACE_LAST : constant := 2;  --  /opt/homebrew/include/libpng16/png.h:688

   PNG_OFFSET_PIXEL : constant := 0;  --  /opt/homebrew/include/libpng16/png.h:691
   PNG_OFFSET_MICROMETER : constant := 1;  --  /opt/homebrew/include/libpng16/png.h:692
   PNG_OFFSET_LAST : constant := 2;  --  /opt/homebrew/include/libpng16/png.h:693

   PNG_EQUATION_LINEAR : constant := 0;  --  /opt/homebrew/include/libpng16/png.h:696
   PNG_EQUATION_BASE_E : constant := 1;  --  /opt/homebrew/include/libpng16/png.h:697
   PNG_EQUATION_ARBITRARY : constant := 2;  --  /opt/homebrew/include/libpng16/png.h:698
   PNG_EQUATION_HYPERBOLIC : constant := 3;  --  /opt/homebrew/include/libpng16/png.h:699
   PNG_EQUATION_LAST : constant := 4;  --  /opt/homebrew/include/libpng16/png.h:700

   PNG_SCALE_UNKNOWN : constant := 0;  --  /opt/homebrew/include/libpng16/png.h:703
   PNG_SCALE_METER : constant := 1;  --  /opt/homebrew/include/libpng16/png.h:704
   PNG_SCALE_RADIAN : constant := 2;  --  /opt/homebrew/include/libpng16/png.h:705
   PNG_SCALE_LAST : constant := 3;  --  /opt/homebrew/include/libpng16/png.h:706

   PNG_RESOLUTION_UNKNOWN : constant := 0;  --  /opt/homebrew/include/libpng16/png.h:709
   PNG_RESOLUTION_METER : constant := 1;  --  /opt/homebrew/include/libpng16/png.h:710
   PNG_RESOLUTION_LAST : constant := 2;  --  /opt/homebrew/include/libpng16/png.h:711

   PNG_sRGB_INTENT_PERCEPTUAL : constant := 0;  --  /opt/homebrew/include/libpng16/png.h:714
   PNG_sRGB_INTENT_RELATIVE : constant := 1;  --  /opt/homebrew/include/libpng16/png.h:715
   PNG_sRGB_INTENT_SATURATION : constant := 2;  --  /opt/homebrew/include/libpng16/png.h:716
   PNG_sRGB_INTENT_ABSOLUTE : constant := 3;  --  /opt/homebrew/include/libpng16/png.h:717
   PNG_sRGB_INTENT_LAST : constant := 4;  --  /opt/homebrew/include/libpng16/png.h:718

   PNG_KEYWORD_MAX_LENGTH : constant := 79;  --  /opt/homebrew/include/libpng16/png.h:721

   PNG_MAX_PALETTE_LENGTH : constant := 256;  --  /opt/homebrew/include/libpng16/png.h:724

   PNG_INFO_gAMA : constant := 16#0001#;  --  /opt/homebrew/include/libpng16/png.h:731
   PNG_INFO_sBIT : constant := 16#0002#;  --  /opt/homebrew/include/libpng16/png.h:732
   PNG_INFO_cHRM : constant := 16#0004#;  --  /opt/homebrew/include/libpng16/png.h:733
   PNG_INFO_PLTE : constant := 16#0008#;  --  /opt/homebrew/include/libpng16/png.h:734
   PNG_INFO_tRNS : constant := 16#0010#;  --  /opt/homebrew/include/libpng16/png.h:735
   PNG_INFO_bKGD : constant := 16#0020#;  --  /opt/homebrew/include/libpng16/png.h:736
   PNG_INFO_hIST : constant := 16#0040#;  --  /opt/homebrew/include/libpng16/png.h:737
   PNG_INFO_pHYs : constant := 16#0080#;  --  /opt/homebrew/include/libpng16/png.h:738
   PNG_INFO_oFFs : constant := 16#0100#;  --  /opt/homebrew/include/libpng16/png.h:739
   PNG_INFO_tIME : constant := 16#0200#;  --  /opt/homebrew/include/libpng16/png.h:740
   PNG_INFO_pCAL : constant := 16#0400#;  --  /opt/homebrew/include/libpng16/png.h:741
   PNG_INFO_sRGB : constant := 16#0800#;  --  /opt/homebrew/include/libpng16/png.h:742
   PNG_INFO_iCCP : constant := 16#1000#;  --  /opt/homebrew/include/libpng16/png.h:743
   PNG_INFO_sPLT : constant := 16#2000#;  --  /opt/homebrew/include/libpng16/png.h:744
   PNG_INFO_sCAL : constant := 16#4000#;  --  /opt/homebrew/include/libpng16/png.h:745
   PNG_INFO_IDAT : constant := 16#8000#;  --  /opt/homebrew/include/libpng16/png.h:746
   PNG_INFO_eXIf : constant := 16#10000#;  --  /opt/homebrew/include/libpng16/png.h:747

   PNG_TRANSFORM_IDENTITY : constant := 16#0000#;  --  /opt/homebrew/include/libpng16/png.h:831
   PNG_TRANSFORM_STRIP_16 : constant := 16#0001#;  --  /opt/homebrew/include/libpng16/png.h:832
   PNG_TRANSFORM_STRIP_ALPHA : constant := 16#0002#;  --  /opt/homebrew/include/libpng16/png.h:833
   PNG_TRANSFORM_PACKING : constant := 16#0004#;  --  /opt/homebrew/include/libpng16/png.h:834
   PNG_TRANSFORM_PACKSWAP : constant := 16#0008#;  --  /opt/homebrew/include/libpng16/png.h:835
   PNG_TRANSFORM_EXPAND : constant := 16#0010#;  --  /opt/homebrew/include/libpng16/png.h:836
   PNG_TRANSFORM_INVERT_MONO : constant := 16#0020#;  --  /opt/homebrew/include/libpng16/png.h:837
   PNG_TRANSFORM_SHIFT : constant := 16#0040#;  --  /opt/homebrew/include/libpng16/png.h:838
   PNG_TRANSFORM_BGR : constant := 16#0080#;  --  /opt/homebrew/include/libpng16/png.h:839
   PNG_TRANSFORM_SWAP_ALPHA : constant := 16#0100#;  --  /opt/homebrew/include/libpng16/png.h:840
   PNG_TRANSFORM_SWAP_ENDIAN : constant := 16#0200#;  --  /opt/homebrew/include/libpng16/png.h:841
   PNG_TRANSFORM_INVERT_ALPHA : constant := 16#0400#;  --  /opt/homebrew/include/libpng16/png.h:842
   PNG_TRANSFORM_STRIP_FILLER : constant := 16#0800#;  --  /opt/homebrew/include/libpng16/png.h:843
   --  unsupported macro: PNG_TRANSFORM_STRIP_FILLER_BEFORE PNG_TRANSFORM_STRIP_FILLER

   PNG_TRANSFORM_STRIP_FILLER_AFTER : constant := 16#1000#;  --  /opt/homebrew/include/libpng16/png.h:846

   PNG_TRANSFORM_GRAY_TO_RGB : constant := 16#2000#;  --  /opt/homebrew/include/libpng16/png.h:848

   PNG_TRANSFORM_EXPAND_16 : constant := 16#4000#;  --  /opt/homebrew/include/libpng16/png.h:850

   PNG_TRANSFORM_SCALE_16 : constant := 16#8000#;  --  /opt/homebrew/include/libpng16/png.h:852

   PNG_FLAG_MNG_EMPTY_PLTE : constant := 16#01#;  --  /opt/homebrew/include/libpng16/png.h:856
   PNG_FLAG_MNG_FILTER_64 : constant := 16#04#;  --  /opt/homebrew/include/libpng16/png.h:857
   PNG_ALL_MNG_FEATURES : constant := 16#05#;  --  /opt/homebrew/include/libpng16/png.h:858
   --  arg-macro: function check_sig (sig, n)
   --    return png_sig_cmp((sig), 0, (n)) = 0;
   --  arg-macro: function jmpbuf (png_ptr)
   --    return *png_set_longjmp_fn((png_ptr), longjmp, (sizeof (jmp_buf)));

   PNG_ERROR_ACTION_NONE : constant := 1;  --  /opt/homebrew/include/libpng16/png.h:1075
   PNG_ERROR_ACTION_WARN : constant := 2;  --  /opt/homebrew/include/libpng16/png.h:1076
   PNG_ERROR_ACTION_ERROR : constant := 3;  --  /opt/homebrew/include/libpng16/png.h:1077
   PNG_RGB_TO_GRAY_DEFAULT : constant := (-1);  --  /opt/homebrew/include/libpng16/png.h:1078

   PNG_ALPHA_PNG : constant := 0;  --  /opt/homebrew/include/libpng16/png.h:1129
   PNG_ALPHA_STANDARD : constant := 1;  --  /opt/homebrew/include/libpng16/png.h:1130
   PNG_ALPHA_ASSOCIATED : constant := 1;  --  /opt/homebrew/include/libpng16/png.h:1131
   PNG_ALPHA_PREMULTIPLIED : constant := 1;  --  /opt/homebrew/include/libpng16/png.h:1132
   PNG_ALPHA_OPTIMIZED : constant := 2;  --  /opt/homebrew/include/libpng16/png.h:1133
   PNG_ALPHA_BROKEN : constant := 3;  --  /opt/homebrew/include/libpng16/png.h:1134

   PNG_DEFAULT_sRGB : constant := -1;  --  /opt/homebrew/include/libpng16/png.h:1146
   PNG_GAMMA_MAC_18 : constant := -2;  --  /opt/homebrew/include/libpng16/png.h:1147
   PNG_GAMMA_sRGB : constant := 220000;  --  /opt/homebrew/include/libpng16/png.h:1148
   --  unsupported macro: PNG_GAMMA_LINEAR PNG_FP_1

   PNG_FILLER_BEFORE : constant := 0;  --  /opt/homebrew/include/libpng16/png.h:1247
   PNG_FILLER_AFTER : constant := 1;  --  /opt/homebrew/include/libpng16/png.h:1248

   PNG_BACKGROUND_GAMMA_UNKNOWN : constant := 0;  --  /opt/homebrew/include/libpng16/png.h:1306
   PNG_BACKGROUND_GAMMA_SCREEN : constant := 1;  --  /opt/homebrew/include/libpng16/png.h:1307
   PNG_BACKGROUND_GAMMA_FILE : constant := 2;  --  /opt/homebrew/include/libpng16/png.h:1308
   PNG_BACKGROUND_GAMMA_UNIQUE : constant := 3;  --  /opt/homebrew/include/libpng16/png.h:1309
   --  unsupported macro: PNG_GAMMA_THRESHOLD (PNG_GAMMA_THRESHOLD_FIXED*.00001)

   PNG_CRC_DEFAULT : constant := 0;  --  /opt/homebrew/include/libpng16/png.h:1435
   PNG_CRC_ERROR_QUIT : constant := 1;  --  /opt/homebrew/include/libpng16/png.h:1436
   PNG_CRC_WARN_DISCARD : constant := 2;  --  /opt/homebrew/include/libpng16/png.h:1437
   PNG_CRC_WARN_USE : constant := 3;  --  /opt/homebrew/include/libpng16/png.h:1438
   PNG_CRC_QUIET_USE : constant := 4;  --  /opt/homebrew/include/libpng16/png.h:1439
   PNG_CRC_NO_CHANGE : constant := 5;  --  /opt/homebrew/include/libpng16/png.h:1440

   PNG_NO_FILTERS : constant := 16#00#;  --  /opt/homebrew/include/libpng16/png.h:1463
   PNG_FILTER_NONE : constant := 16#08#;  --  /opt/homebrew/include/libpng16/png.h:1464
   PNG_FILTER_SUB : constant := 16#10#;  --  /opt/homebrew/include/libpng16/png.h:1465
   PNG_FILTER_UP : constant := 16#20#;  --  /opt/homebrew/include/libpng16/png.h:1466
   PNG_FILTER_AVG : constant := 16#40#;  --  /opt/homebrew/include/libpng16/png.h:1467
   PNG_FILTER_PAETH : constant := 16#80#;  --  /opt/homebrew/include/libpng16/png.h:1468
   --  unsupported macro: PNG_FAST_FILTERS (PNG_FILTER_NONE | PNG_FILTER_SUB | PNG_FILTER_UP)
   --  unsupported macro: PNG_ALL_FILTERS (PNG_FAST_FILTERS | PNG_FILTER_AVG | PNG_FILTER_PAETH)

   PNG_FILTER_VALUE_NONE : constant := 0;  --  /opt/homebrew/include/libpng16/png.h:1475
   PNG_FILTER_VALUE_SUB : constant := 1;  --  /opt/homebrew/include/libpng16/png.h:1476
   PNG_FILTER_VALUE_UP : constant := 2;  --  /opt/homebrew/include/libpng16/png.h:1477
   PNG_FILTER_VALUE_AVG : constant := 3;  --  /opt/homebrew/include/libpng16/png.h:1478
   PNG_FILTER_VALUE_PAETH : constant := 4;  --  /opt/homebrew/include/libpng16/png.h:1479
   PNG_FILTER_VALUE_LAST : constant := 5;  --  /opt/homebrew/include/libpng16/png.h:1480

   PNG_FILTER_HEURISTIC_DEFAULT : constant := 0;  --  /opt/homebrew/include/libpng16/png.h:1494
   PNG_FILTER_HEURISTIC_UNWEIGHTED : constant := 1;  --  /opt/homebrew/include/libpng16/png.h:1495
   PNG_FILTER_HEURISTIC_WEIGHTED : constant := 2;  --  /opt/homebrew/include/libpng16/png.h:1496
   PNG_FILTER_HEURISTIC_LAST : constant := 3;  --  /opt/homebrew/include/libpng16/png.h:1497

   PNG_DESTROY_WILL_FREE_DATA : constant := 1;  --  /opt/homebrew/include/libpng16/png.h:1740
   PNG_SET_WILL_FREE_DATA : constant := 1;  --  /opt/homebrew/include/libpng16/png.h:1741
   PNG_USER_WILL_FREE_DATA : constant := 2;  --  /opt/homebrew/include/libpng16/png.h:1742

   PNG_FREE_HIST : constant := 16#0008#;  --  /opt/homebrew/include/libpng16/png.h:1744
   PNG_FREE_ICCP : constant := 16#0010#;  --  /opt/homebrew/include/libpng16/png.h:1745
   PNG_FREE_SPLT : constant := 16#0020#;  --  /opt/homebrew/include/libpng16/png.h:1746
   PNG_FREE_ROWS : constant := 16#0040#;  --  /opt/homebrew/include/libpng16/png.h:1747
   PNG_FREE_PCAL : constant := 16#0080#;  --  /opt/homebrew/include/libpng16/png.h:1748
   PNG_FREE_SCAL : constant := 16#0100#;  --  /opt/homebrew/include/libpng16/png.h:1749

   PNG_FREE_UNKN : constant := 16#0200#;  --  /opt/homebrew/include/libpng16/png.h:1751

   PNG_FREE_PLTE : constant := 16#1000#;  --  /opt/homebrew/include/libpng16/png.h:1754
   PNG_FREE_TRNS : constant := 16#2000#;  --  /opt/homebrew/include/libpng16/png.h:1755
   PNG_FREE_TEXT : constant := 16#4000#;  --  /opt/homebrew/include/libpng16/png.h:1756
   PNG_FREE_EXIF : constant := 16#8000#;  --  /opt/homebrew/include/libpng16/png.h:1757
   PNG_FREE_ALL : constant := 16#ffff#;  --  /opt/homebrew/include/libpng16/png.h:1758
   PNG_FREE_MUL : constant := 16#4220#;  --  /opt/homebrew/include/libpng16/png.h:1759

   PNG_HANDLE_CHUNK_AS_DEFAULT : constant := 0;  --  /opt/homebrew/include/libpng16/png.h:2341
   PNG_HANDLE_CHUNK_NEVER : constant := 1;  --  /opt/homebrew/include/libpng16/png.h:2342
   PNG_HANDLE_CHUNK_IF_SAFE : constant := 2;  --  /opt/homebrew/include/libpng16/png.h:2343
   PNG_HANDLE_CHUNK_ALWAYS : constant := 3;  --  /opt/homebrew/include/libpng16/png.h:2344
   PNG_HANDLE_CHUNK_LAST : constant := 4;  --  /opt/homebrew/include/libpng16/png.h:2345

   PNG_IO_NONE : constant := 16#0000#;  --  /opt/homebrew/include/libpng16/png.h:2418
   PNG_IO_READING : constant := 16#0001#;  --  /opt/homebrew/include/libpng16/png.h:2419
   PNG_IO_WRITING : constant := 16#0002#;  --  /opt/homebrew/include/libpng16/png.h:2420
   PNG_IO_SIGNATURE : constant := 16#0010#;  --  /opt/homebrew/include/libpng16/png.h:2421
   PNG_IO_CHUNK_HDR : constant := 16#0020#;  --  /opt/homebrew/include/libpng16/png.h:2422
   PNG_IO_CHUNK_DATA : constant := 16#0040#;  --  /opt/homebrew/include/libpng16/png.h:2423
   PNG_IO_CHUNK_CRC : constant := 16#0080#;  --  /opt/homebrew/include/libpng16/png.h:2424
   PNG_IO_MASK_OP : constant := 16#000f#;  --  /opt/homebrew/include/libpng16/png.h:2425
   PNG_IO_MASK_LOC : constant := 16#00f0#;  --  /opt/homebrew/include/libpng16/png.h:2426

   PNG_INTERLACE_ADAM7_PASSES : constant := 7;  --  /opt/homebrew/include/libpng16/png.h:2433
   --  arg-macro: function PASS_START_ROW (pass)
   --    return ((1and~(pass))<<(3-((pass)>>1)))and7;
   --  arg-macro: function PASS_START_COL (pass)
   --    return ((1and (pass))<<(3-(((pass)+1)>>1)))and7;
   --  arg-macro: function PASS_ROW_OFFSET (pass)
   --    return (pass)>2?(8>>(((pass)-1)>>1)):8;
   --  arg-macro: function PASS_COL_OFFSET (pass)
   --    return 2**((7-(pass))>>1);
   --  arg-macro: function PASS_ROW_SHIFT (pass)
   --    return (pass)>2?(8-(pass))>>1:3;
   --  arg-macro: function PASS_COL_SHIFT (pass)
   --    return (pass)>1?(7-(pass))>>1:3;
   --  arg-macro: function PASS_ROWS (height, pass)
   --    return ((height)+(((2**PNG_PASS_ROW_SHIFT(pass)) -1)-PNG_PASS_START_ROW(pass)))>>PNG_PASS_ROW_SHIFT(pass);
   --  arg-macro: function PASS_COLS (width, pass)
   --    return ((width)+(((2**PNG_PASS_COL_SHIFT(pass)) -1)-PNG_PASS_START_COL(pass)))>>PNG_PASS_COL_SHIFT(pass);
   --  arg-macro: function ROW_FROM_PASS_ROW (y_in, pass)
   --    return ((y_in)<<PNG_PASS_ROW_SHIFT(pass))+PNG_PASS_START_ROW(pass);
   --  arg-macro: function COL_FROM_PASS_COL (x_in, pass)
   --    return ((x_in)<<PNG_PASS_COL_SHIFT(pass))+PNG_PASS_START_COL(pass);
   --  arg-macro: function PASS_MASK (pass, off)
   --    return  ((16#110145AF#>>(((7-(off))-(pass))<<2)) and 16#F#) or ((16#01145AF0#>>(((7-(off))-(pass))<<2)) and 16#F0#);
   --  arg-macro: function ROW_IN_INTERLACE_PASS (y, pass)
   --    return (PNG_PASS_MASK(pass,0) >> ((y)and7)) and 1;
   --  arg-macro: function COL_IN_INTERLACE_PASS (x, pass)
   --    return (PNG_PASS_MASK(pass,1) >> ((x)and7)) and 1;
   --  arg-macro: procedure composite (composite, fg, alpha, bg)
   --    { png_uint_16 temp := (png_uint_16)((png_uint_16)(fg) * (png_uint_16)(alpha) + (png_uint_16)(bg)*(png_uint_16)(255 - (png_uint_16)(alpha)) + 128); (composite) := (png_byte)(((temp + (temp >> 8)) >> 8) and 16#ff#); }
   --  arg-macro: procedure composite_16 (composite, fg, alpha, bg)
   --    { png_uint_32 temp := (png_uint_32)((png_uint_32)(fg) * (png_uint_32)(alpha) + (png_uint_32)(bg)*(65535 - (png_uint_32)(alpha)) + 32768); (composite) := (png_uint_16)(16#ffff# and ((temp + (temp >> 16)) >> 16)); }
   --  arg-macro: function get_uint_32 (buf)
   --    return ((png_uint_32)(*(buf)) << 24) + ((png_uint_32)(*((buf) + 1)) << 16) + ((png_uint_32)(*((buf) + 2)) << 8) + ((png_uint_32)(*((buf) + 3)));
   --  arg-macro: function get_uint_16 (buf)
   --    return (png_uint_16) (((unsigned int)(*(buf)) << 8) + ((unsigned int)(*((buf) + 1))));
   --  arg-macro: function get_int_32 (buf)
   --    return (png_int_32)((*(buf) and 16#80#) ? -((png_int_32)(((png_get_uint_32(buf)xor16#ffffffff#)+1)and16#7fffffff#)) : (png_int_32)png_get_uint_32(buf));
   --  arg-macro: procedure get_uint_32 (buf)
   --    PNG_get_uint_32(buf)
   --  arg-macro: procedure get_uint_16 (buf)
   --    PNG_get_uint_16(buf)
   --  arg-macro: procedure get_int_32 (buf)
   --    PNG_get_int_32(buf)

   PNG_IMAGE_VERSION : constant := 1;  --  /opt/homebrew/include/libpng16/png.h:2665

   PNG_IMAGE_WARNING : constant := 1;  --  /opt/homebrew/include/libpng16/png.h:2688
   PNG_IMAGE_ERROR : constant := 2;  --  /opt/homebrew/include/libpng16/png.h:2689
   --  arg-macro: function IMAGE_FAILED (png_cntrl)
   --    return (((png_cntrl).warning_or_error)and16#03#)>1;

   PNG_FORMAT_FLAG_ALPHA : constant := 16#01#;  --  /opt/homebrew/include/libpng16/png.h:2774
   PNG_FORMAT_FLAG_COLOR : constant := 16#02#;  --  /opt/homebrew/include/libpng16/png.h:2775
   PNG_FORMAT_FLAG_LINEAR : constant := 16#04#;  --  /opt/homebrew/include/libpng16/png.h:2776
   PNG_FORMAT_FLAG_COLORMAP : constant := 16#08#;  --  /opt/homebrew/include/libpng16/png.h:2777

   PNG_FORMAT_FLAG_BGR : constant := 16#10#;  --  /opt/homebrew/include/libpng16/png.h:2780

   PNG_FORMAT_FLAG_AFIRST : constant := 16#20#;  --  /opt/homebrew/include/libpng16/png.h:2784

   PNG_FORMAT_FLAG_ASSOCIATED_ALPHA : constant := 16#40#;  --  /opt/homebrew/include/libpng16/png.h:2787

   PNG_FORMAT_GRAY : constant := 0;  --  /opt/homebrew/include/libpng16/png.h:2793
   --  unsupported macro: PNG_FORMAT_GA PNG_FORMAT_FLAG_ALPHA
   --  unsupported macro: PNG_FORMAT_AG (PNG_FORMAT_GA|PNG_FORMAT_FLAG_AFIRST)
   --  unsupported macro: PNG_FORMAT_RGB PNG_FORMAT_FLAG_COLOR
   --  unsupported macro: PNG_FORMAT_BGR (PNG_FORMAT_FLAG_COLOR|PNG_FORMAT_FLAG_BGR)
   --  unsupported macro: PNG_FORMAT_RGBA (PNG_FORMAT_RGB|PNG_FORMAT_FLAG_ALPHA)
   --  unsupported macro: PNG_FORMAT_ARGB (PNG_FORMAT_RGBA|PNG_FORMAT_FLAG_AFIRST)
   --  unsupported macro: PNG_FORMAT_BGRA (PNG_FORMAT_BGR|PNG_FORMAT_FLAG_ALPHA)
   --  unsupported macro: PNG_FORMAT_ABGR (PNG_FORMAT_BGRA|PNG_FORMAT_FLAG_AFIRST)
   --  unsupported macro: PNG_FORMAT_LINEAR_Y PNG_FORMAT_FLAG_LINEAR
   --  unsupported macro: PNG_FORMAT_LINEAR_Y_ALPHA (PNG_FORMAT_FLAG_LINEAR|PNG_FORMAT_FLAG_ALPHA)
   --  unsupported macro: PNG_FORMAT_LINEAR_RGB (PNG_FORMAT_FLAG_LINEAR|PNG_FORMAT_FLAG_COLOR)
   --  unsupported macro: PNG_FORMAT_LINEAR_RGB_ALPHA (PNG_FORMAT_FLAG_LINEAR|PNG_FORMAT_FLAG_COLOR|PNG_FORMAT_FLAG_ALPHA)
   --  unsupported macro: PNG_FORMAT_RGB_COLORMAP (PNG_FORMAT_RGB|PNG_FORMAT_FLAG_COLORMAP)
   --  unsupported macro: PNG_FORMAT_BGR_COLORMAP (PNG_FORMAT_BGR|PNG_FORMAT_FLAG_COLORMAP)
   --  unsupported macro: PNG_FORMAT_RGBA_COLORMAP (PNG_FORMAT_RGBA|PNG_FORMAT_FLAG_COLORMAP)
   --  unsupported macro: PNG_FORMAT_ARGB_COLORMAP (PNG_FORMAT_ARGB|PNG_FORMAT_FLAG_COLORMAP)
   --  unsupported macro: PNG_FORMAT_BGRA_COLORMAP (PNG_FORMAT_BGRA|PNG_FORMAT_FLAG_COLORMAP)
   --  unsupported macro: PNG_FORMAT_ABGR_COLORMAP (PNG_FORMAT_ABGR|PNG_FORMAT_FLAG_COLORMAP)
   --  arg-macro: function IMAGE_SAMPLE_CHANNELS (fmt)
   --    return ((fmt)and(PNG_FORMAT_FLAG_COLORorPNG_FORMAT_FLAG_ALPHA))+1;
   --  arg-macro: function IMAGE_SAMPLE_COMPONENT_SIZE (fmt)
   --    return (((fmt) and PNG_FORMAT_FLAG_LINEAR) >> 2)+1;
   --  arg-macro: function IMAGE_SAMPLE_SIZE (fmt)
   --    return PNG_IMAGE_SAMPLE_CHANNELS(fmt) * PNG_IMAGE_SAMPLE_COMPONENT_SIZE(fmt);
   --  arg-macro: function IMAGE_MAXIMUM_COLORMAP_COMPONENTS (fmt)
   --    return PNG_IMAGE_SAMPLE_CHANNELS(fmt) * 256;
   --  arg-macro: function IMAGE_PIXEL_ (test, fmt)
   --    return ((fmt)andPNG_FORMAT_FLAG_COLORMAP)?1:test(fmt);
   --  arg-macro: procedure IMAGE_PIXEL_CHANNELS (fmt)
   --    PNG_IMAGE_PIXEL_(PNG_IMAGE_SAMPLE_CHANNELS,fmt)
   --  arg-macro: procedure IMAGE_PIXEL_COMPONENT_SIZE (fmt)
   --    PNG_IMAGE_PIXEL_(PNG_IMAGE_SAMPLE_COMPONENT_SIZE,fmt)
   --  arg-macro: procedure IMAGE_PIXEL_SIZE (fmt)
   --    PNG_IMAGE_PIXEL_(PNG_IMAGE_SAMPLE_SIZE,fmt)
   --  arg-macro: function IMAGE_ROW_STRIDE (image)
   --    return PNG_IMAGE_PIXEL_CHANNELS((image).format) * (image).width;
   --  arg-macro: function IMAGE_BUFFER_SIZE (image, row_stride)
   --    return PNG_IMAGE_PIXEL_COMPONENT_SIZE((image).format)*(image).height*(row_stride);
   --  arg-macro: procedure IMAGE_SIZE (image)
   --    PNG_IMAGE_BUFFER_SIZE(image, PNG_IMAGE_ROW_STRIDE(image))
   --  arg-macro: function IMAGE_COLORMAP_SIZE (image)
   --    return PNG_IMAGE_SAMPLE_SIZE((image).format) * (image).colormap_entries;

   PNG_IMAGE_FLAG_COLORSPACE_NOT_sRGB : constant := 16#01#;  --  /opt/homebrew/include/libpng16/png.h:2934

   PNG_IMAGE_FLAG_FAST : constant := 16#02#;  --  /opt/homebrew/include/libpng16/png.h:2939

   PNG_IMAGE_FLAG_16BIT_sRGB : constant := 16#04#;  --  /opt/homebrew/include/libpng16/png.h:2950
   --  arg-macro: procedure image_write_get_memory_size (image, size, convert_to_8_bit, buffer, row_stride, colormap)
   --    png_image_write_to_memory(and(image), 0, and(size), convert_to_8_bit, buffer, row_stride, colormap)
   --  arg-macro: function IMAGE_DATA_SIZE (image)
   --    return PNG_IMAGE_SIZE(image)+(image).height;
   --  arg-macro: function ZLIB_MAX_SIZE (b)
   --    return (b)+(((b)+7)>>3)+(((b)+63)>>6)+11;
   --  arg-macro: procedure IMAGE_COMPRESSED_SIZE_MAX (image)
   --    PNG_ZLIB_MAX_SIZE((png_alloc_size_t)PNG_IMAGE_DATA_SIZE(image))
   --  arg-macro: function IMAGE_PNG_SIZE_MAX_ (image, image_size)
   --    return (8 +25 +16 +44 +12 + (((image).formatandPNG_FORMAT_FLAG_COLORMAP)? 12+3*(image).colormap_entries + (((image).formatandPNG_FORMAT_FLAG_ALPHA)? 12 +(image).colormap_entries:0):0)+ 12)+(12*((image_size)/PNG_ZBUF_SIZE)) +(image_size);
   --  arg-macro: procedure IMAGE_PNG_SIZE_MAX (image)
   --    PNG_IMAGE_PNG_SIZE_MAX_(image, PNG_IMAGE_COMPRESSED_SIZE_MAX(image))

   PNG_MAXIMUM_INFLATE_WINDOW : constant := 2;  --  /opt/homebrew/include/libpng16/png.h:3201
   PNG_SKIP_sRGB_CHECK_PROFILE : constant := 4;  --  /opt/homebrew/include/libpng16/png.h:3202

   PNG_OPTION_NEXT : constant := 14;  --  /opt/homebrew/include/libpng16/png.h:3217

   PNG_OPTION_UNSET : constant := 0;  --  /opt/homebrew/include/libpng16/png.h:3220
   PNG_OPTION_INVALID : constant := 1;  --  /opt/homebrew/include/libpng16/png.h:3221
   PNG_OPTION_OFF : constant := 2;  --  /opt/homebrew/include/libpng16/png.h:3222
   PNG_OPTION_ON : constant := 3;  --  /opt/homebrew/include/libpng16/png.h:3223

   type png_libpng_version_1_6_43 is new Interfaces.C.Strings.chars_ptr;  -- /opt/homebrew/include/libpng16/png.h:430

   type png_struct_def is null record;   -- incomplete struct

   subtype png_struct is png_struct_def;  -- /opt/homebrew/include/libpng16/png.h:438

   type png_const_structp is access constant png_struct;  -- /opt/homebrew/include/libpng16/png.h:439

   type png_structp is access all png_struct;  -- /opt/homebrew/include/libpng16/png.h:440

   type png_structpp is new System.Address;  -- /opt/homebrew/include/libpng16/png.h:441

   type png_info_def is null record;   -- incomplete struct

   subtype png_info is png_info_def;  -- /opt/homebrew/include/libpng16/png.h:452

   type png_infop is access all png_info;  -- /opt/homebrew/include/libpng16/png.h:453

   type png_const_infop is access constant png_info;  -- /opt/homebrew/include/libpng16/png.h:454

   type png_infopp is new System.Address;  -- /opt/homebrew/include/libpng16/png.h:455

   type png_structrp is access all png_struct;  -- /opt/homebrew/include/libpng16/png.h:468

   type png_const_structrp is access constant png_struct;  -- /opt/homebrew/include/libpng16/png.h:469

   type png_inforp is access all png_info;  -- /opt/homebrew/include/libpng16/png.h:470

   type png_const_inforp is access constant png_info;  -- /opt/homebrew/include/libpng16/png.h:471

   type png_color_struct is record
      red : aliased adalibpng.conf.png_byte;  -- /opt/homebrew/include/libpng16/png.h:479
      green : aliased adalibpng.conf.png_byte;  -- /opt/homebrew/include/libpng16/png.h:480
      blue : aliased adalibpng.conf.png_byte;  -- /opt/homebrew/include/libpng16/png.h:481
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/libpng16/png.h:477

   subtype png_color is png_color_struct;  -- /opt/homebrew/include/libpng16/png.h:482

   type png_colorp is access all png_color;  -- /opt/homebrew/include/libpng16/png.h:483

   type png_const_colorp is access constant png_color;  -- /opt/homebrew/include/libpng16/png.h:484

   type png_colorpp is new System.Address;  -- /opt/homebrew/include/libpng16/png.h:485

   type png_color_16_struct is record
      index : aliased adalibpng.conf.png_byte;  -- /opt/homebrew/include/libpng16/png.h:489
      red : aliased adalibpng.conf.png_uint_16;  -- /opt/homebrew/include/libpng16/png.h:490
      green : aliased adalibpng.conf.png_uint_16;  -- /opt/homebrew/include/libpng16/png.h:491
      blue : aliased adalibpng.conf.png_uint_16;  -- /opt/homebrew/include/libpng16/png.h:492
      gray : aliased adalibpng.conf.png_uint_16;  -- /opt/homebrew/include/libpng16/png.h:493
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/libpng16/png.h:487

   subtype png_color_16 is png_color_16_struct;  -- /opt/homebrew/include/libpng16/png.h:494

   type png_color_16p is access all png_color_16;  -- /opt/homebrew/include/libpng16/png.h:495

   type png_const_color_16p is access constant png_color_16;  -- /opt/homebrew/include/libpng16/png.h:496

   type png_color_16pp is new System.Address;  -- /opt/homebrew/include/libpng16/png.h:497

   type png_color_8_struct is record
      red : aliased adalibpng.conf.png_byte;  -- /opt/homebrew/include/libpng16/png.h:501
      green : aliased adalibpng.conf.png_byte;  -- /opt/homebrew/include/libpng16/png.h:502
      blue : aliased adalibpng.conf.png_byte;  -- /opt/homebrew/include/libpng16/png.h:503
      gray : aliased adalibpng.conf.png_byte;  -- /opt/homebrew/include/libpng16/png.h:504
      alpha : aliased adalibpng.conf.png_byte;  -- /opt/homebrew/include/libpng16/png.h:505
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/libpng16/png.h:499

   subtype png_color_8 is png_color_8_struct;  -- /opt/homebrew/include/libpng16/png.h:506

   type png_color_8p is access all png_color_8;  -- /opt/homebrew/include/libpng16/png.h:507

   type png_const_color_8p is access constant png_color_8;  -- /opt/homebrew/include/libpng16/png.h:508

   type png_color_8pp is new System.Address;  -- /opt/homebrew/include/libpng16/png.h:509

   type png_sPLT_entry_struct is record
      red : aliased adalibpng.conf.png_uint_16;  -- /opt/homebrew/include/libpng16/png.h:517
      green : aliased adalibpng.conf.png_uint_16;  -- /opt/homebrew/include/libpng16/png.h:518
      blue : aliased adalibpng.conf.png_uint_16;  -- /opt/homebrew/include/libpng16/png.h:519
      alpha : aliased adalibpng.conf.png_uint_16;  -- /opt/homebrew/include/libpng16/png.h:520
      frequency : aliased adalibpng.conf.png_uint_16;  -- /opt/homebrew/include/libpng16/png.h:521
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/libpng16/png.h:515

   subtype png_sPLT_entry is png_sPLT_entry_struct;  -- /opt/homebrew/include/libpng16/png.h:522

   type png_sPLT_entryp is access all png_sPLT_entry;  -- /opt/homebrew/include/libpng16/png.h:523

   type png_const_sPLT_entryp is access constant png_sPLT_entry;  -- /opt/homebrew/include/libpng16/png.h:524

   type png_sPLT_entrypp is new System.Address;  -- /opt/homebrew/include/libpng16/png.h:525

   type png_sPLT_struct is record
      name : adalibpng.conf.png_charp;  -- /opt/homebrew/include/libpng16/png.h:534
      depth : aliased adalibpng.conf.png_byte;  -- /opt/homebrew/include/libpng16/png.h:535
      entries : png_sPLT_entryp;  -- /opt/homebrew/include/libpng16/png.h:536
      nentries : aliased adalibpng.conf.png_int_32;  -- /opt/homebrew/include/libpng16/png.h:537
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/libpng16/png.h:532

   subtype png_sPLT_t is png_sPLT_struct;  -- /opt/homebrew/include/libpng16/png.h:538

   type png_sPLT_tp is access all png_sPLT_t;  -- /opt/homebrew/include/libpng16/png.h:539

   type png_const_sPLT_tp is access constant png_sPLT_t;  -- /opt/homebrew/include/libpng16/png.h:540

   type png_sPLT_tpp is new System.Address;  -- /opt/homebrew/include/libpng16/png.h:541

   type png_text_struct is record
      compression : aliased int;  -- /opt/homebrew/include/libpng16/png.h:563
      key : adalibpng.conf.png_charp;  -- /opt/homebrew/include/libpng16/png.h:568
      text : adalibpng.conf.png_charp;  -- /opt/homebrew/include/libpng16/png.h:569
      text_length : aliased size_t;  -- /opt/homebrew/include/libpng16/png.h:571
      itxt_length : aliased size_t;  -- /opt/homebrew/include/libpng16/png.h:572
      lang : adalibpng.conf.png_charp;  -- /opt/homebrew/include/libpng16/png.h:573
      lang_key : adalibpng.conf.png_charp;  -- /opt/homebrew/include/libpng16/png.h:575
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/libpng16/png.h:561

   subtype png_text is png_text_struct;  -- /opt/homebrew/include/libpng16/png.h:577

   type png_textp is access all png_text;  -- /opt/homebrew/include/libpng16/png.h:578

   type png_const_textp is access constant png_text;  -- /opt/homebrew/include/libpng16/png.h:579

   type png_textpp is new System.Address;  -- /opt/homebrew/include/libpng16/png.h:580

   type png_time_struct is record
      year : aliased adalibpng.conf.png_uint_16;  -- /opt/homebrew/include/libpng16/png.h:601
      month : aliased adalibpng.conf.png_byte;  -- /opt/homebrew/include/libpng16/png.h:602
      day : aliased adalibpng.conf.png_byte;  -- /opt/homebrew/include/libpng16/png.h:603
      hour : aliased adalibpng.conf.png_byte;  -- /opt/homebrew/include/libpng16/png.h:604
      minute : aliased adalibpng.conf.png_byte;  -- /opt/homebrew/include/libpng16/png.h:605
      second : aliased adalibpng.conf.png_byte;  -- /opt/homebrew/include/libpng16/png.h:606
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/libpng16/png.h:599

   subtype png_time is png_time_struct;  -- /opt/homebrew/include/libpng16/png.h:607

   type png_timep is access all png_time;  -- /opt/homebrew/include/libpng16/png.h:608

   type png_const_timep is access constant png_time;  -- /opt/homebrew/include/libpng16/png.h:609

   type png_timepp is new System.Address;  -- /opt/homebrew/include/libpng16/png.h:610

   type anon_array1546 is array (0 .. 4) of aliased adalibpng.conf.png_byte;
   type png_unknown_chunk_t is record
      name : aliased anon_array1546;  -- /opt/homebrew/include/libpng16/png.h:623
      data : access adalibpng.conf.png_byte;  -- /opt/homebrew/include/libpng16/png.h:624
      size : aliased size_t;  -- /opt/homebrew/include/libpng16/png.h:625
      location : aliased adalibpng.conf.png_byte;  -- /opt/homebrew/include/libpng16/png.h:633
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/libpng16/png.h:621

   subtype png_unknown_chunk is png_unknown_chunk_t;  -- /opt/homebrew/include/libpng16/png.h:635

   type png_unknown_chunkp is access all png_unknown_chunk;  -- /opt/homebrew/include/libpng16/png.h:637

   type png_const_unknown_chunkp is access constant png_unknown_chunk;  -- /opt/homebrew/include/libpng16/png.h:638

   type png_unknown_chunkpp is new System.Address;  -- /opt/homebrew/include/libpng16/png.h:639

   type png_row_info_struct is record
      width : aliased adalibpng.conf.png_uint_32;  -- /opt/homebrew/include/libpng16/png.h:755
      rowbytes : aliased size_t;  -- /opt/homebrew/include/libpng16/png.h:756
      color_type : aliased adalibpng.conf.png_byte;  -- /opt/homebrew/include/libpng16/png.h:757
      bit_depth : aliased adalibpng.conf.png_byte;  -- /opt/homebrew/include/libpng16/png.h:758
      channels : aliased adalibpng.conf.png_byte;  -- /opt/homebrew/include/libpng16/png.h:759
      pixel_depth : aliased adalibpng.conf.png_byte;  -- /opt/homebrew/include/libpng16/png.h:760
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/libpng16/png.h:753

   subtype png_row_info is png_row_info_struct;  -- /opt/homebrew/include/libpng16/png.h:761

   type png_row_infop is access all png_row_info;  -- /opt/homebrew/include/libpng16/png.h:763

   type png_row_infopp is new System.Address;  -- /opt/homebrew/include/libpng16/png.h:764

   type png_error_ptr is access procedure (arg1 : png_structp; arg2 : adalibpng.conf.png_const_charp)
   with Convention => C;  -- /opt/homebrew/include/libpng16/png.h:774

   type png_rw_ptr is access procedure
        (arg1 : png_structp;
         arg2 : adalibpng.conf.png_bytep;
         arg3 : size_t)
   with Convention => C;  -- /opt/homebrew/include/libpng16/png.h:775

   type png_flush_ptr is access procedure (arg1 : png_structp)
   with Convention => C;  -- /opt/homebrew/include/libpng16/png.h:776

   type png_read_status_ptr is access procedure
        (arg1 : png_structp;
         arg2 : adalibpng.conf.png_uint_32;
         arg3 : int)
   with Convention => C;  -- /opt/homebrew/include/libpng16/png.h:777

   type png_write_status_ptr is access procedure
        (arg1 : png_structp;
         arg2 : adalibpng.conf.png_uint_32;
         arg3 : int)
   with Convention => C;  -- /opt/homebrew/include/libpng16/png.h:779

   type png_progressive_info_ptr is access procedure (arg1 : png_structp; arg2 : png_infop)
   with Convention => C;  -- /opt/homebrew/include/libpng16/png.h:783

   type png_progressive_end_ptr is access procedure (arg1 : png_structp; arg2 : png_infop)
   with Convention => C;  -- /opt/homebrew/include/libpng16/png.h:784

   type png_progressive_row_ptr is access procedure
        (arg1 : png_structp;
         arg2 : adalibpng.conf.png_bytep;
         arg3 : adalibpng.conf.png_uint_32;
         arg4 : int)
   with Convention => C;  -- /opt/homebrew/include/libpng16/png.h:796

   type png_user_transform_ptr is access procedure
        (arg1 : png_structp;
         arg2 : png_row_infop;
         arg3 : adalibpng.conf.png_bytep)
   with Convention => C;  -- /opt/homebrew/include/libpng16/png.h:802

   type png_user_chunk_ptr is access function (arg1 : png_structp; arg2 : png_unknown_chunkp) return int
   with Convention => C;  -- /opt/homebrew/include/libpng16/png.h:807

   type png_longjmp_ptr is access procedure (arg1 : access int; arg2 : int)
   with Convention => C;  -- /opt/homebrew/include/libpng16/png.h:827

   type png_malloc_ptr is access function (arg1 : png_structp; arg2 : adalibpng.conf.png_alloc_size_t) return adalibpng.conf.png_voidp
   with Convention => C;  -- /opt/homebrew/include/libpng16/png.h:866

   type png_free_ptr is access procedure (arg1 : png_structp; arg2 : adalibpng.conf.png_voidp)
   with Convention => C;  -- /opt/homebrew/include/libpng16/png.h:868

   function access_version_number return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:900
   with Import => True, 
        Convention => C, 
        External_Name => "png_access_version_number";

   procedure set_sig_bytes (png_ptr : png_structrp; num_bytes : int)  -- /opt/homebrew/include/libpng16/png.h:905
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_sig_bytes";

   function sig_cmp
     (sig : adalibpng.conf.png_const_bytep;
      start : size_t;
      num_to_check : size_t) return int  -- /opt/homebrew/include/libpng16/png.h:912
   with Import => True, 
        Convention => C, 
        External_Name => "png_sig_cmp";

   function create_read_struct
     (user_png_ver : adalibpng.conf.png_const_charp;
      error_ptr : adalibpng.conf.png_voidp;
      error_fn : png_error_ptr;
      warn_fn : png_error_ptr) return png_structp  -- /opt/homebrew/include/libpng16/png.h:921
   with Import => True, 
        Convention => C, 
        External_Name => "png_create_read_struct";

   function create_write_struct
     (user_png_ver : adalibpng.conf.png_const_charp;
      error_ptr : adalibpng.conf.png_voidp;
      error_fn : png_error_ptr;
      warn_fn : png_error_ptr) return png_structp  -- /opt/homebrew/include/libpng16/png.h:927
   with Import => True, 
        Convention => C, 
        External_Name => "png_create_write_struct";

   function get_compression_buffer_size (png_ptr : png_const_structrp) return size_t  -- /opt/homebrew/include/libpng16/png.h:932
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_compression_buffer_size";

   procedure set_compression_buffer_size (png_ptr : png_structrp; size : size_t)  -- /opt/homebrew/include/libpng16/png.h:935
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_compression_buffer_size";

   --function set_longjmp_fn
    -- (png_ptr : png_structrp;
    --  longjmp_fn : png_longjmp_ptr;
    --  jmp_buf_size : size_t) return access setjmp_h.jmp_buf  -- /opt/homebrew/include/libpng16/png.h:949
   --with Import => True, 
    --    Convention => C, 
    --    External_Name => "png_set_longjmp_fn";

   procedure longjmp (png_ptr : png_const_structrp; val : int)  -- /opt/homebrew/include/libpng16/png.h:962
   with Import => True, 
        Convention => C, 
        External_Name => "png_longjmp";

   function reset_zstream (png_ptr : png_structrp) return int  -- /opt/homebrew/include/libpng16/png.h:967
   with Import => True, 
        Convention => C, 
        External_Name => "png_reset_zstream";

   function create_read_struct_2
     (user_png_ver : adalibpng.conf.png_const_charp;
      error_ptr : adalibpng.conf.png_voidp;
      error_fn : png_error_ptr;
      warn_fn : png_error_ptr;
      mem_ptr : adalibpng.conf.png_voidp;
      malloc_fn : png_malloc_ptr;
      free_fn : png_free_ptr) return png_structp  -- /opt/homebrew/include/libpng16/png.h:972
   with Import => True, 
        Convention => C, 
        External_Name => "png_create_read_struct_2";

   function create_write_struct_2
     (user_png_ver : adalibpng.conf.png_const_charp;
      error_ptr : adalibpng.conf.png_voidp;
      error_fn : png_error_ptr;
      warn_fn : png_error_ptr;
      mem_ptr : adalibpng.conf.png_voidp;
      malloc_fn : png_malloc_ptr;
      free_fn : png_free_ptr) return png_structp  -- /opt/homebrew/include/libpng16/png.h:977
   with Import => True, 
        Convention => C, 
        External_Name => "png_create_write_struct_2";

   procedure write_sig (png_ptr : png_structrp)  -- /opt/homebrew/include/libpng16/png.h:985
   with Import => True, 
        Convention => C, 
        External_Name => "png_write_sig";

   procedure write_chunk
     (png_ptr : png_structrp;
      chunk_name : adalibpng.conf.png_const_bytep;
      data : adalibpng.conf.png_const_bytep;
      length : size_t)  -- /opt/homebrew/include/libpng16/png.h:988
   with Import => True, 
        Convention => C, 
        External_Name => "png_write_chunk";

   procedure write_chunk_start
     (png_ptr : png_structrp;
      chunk_name : adalibpng.conf.png_const_bytep;
      length : adalibpng.conf.png_uint_32)  -- /opt/homebrew/include/libpng16/png.h:992
   with Import => True, 
        Convention => C, 
        External_Name => "png_write_chunk_start";

   procedure write_chunk_data
     (png_ptr : png_structrp;
      data : adalibpng.conf.png_const_bytep;
      length : size_t)  -- /opt/homebrew/include/libpng16/png.h:996
   with Import => True, 
        Convention => C, 
        External_Name => "png_write_chunk_data";

   procedure write_chunk_end (png_ptr : png_structrp)  -- /opt/homebrew/include/libpng16/png.h:1000
   with Import => True, 
        Convention => C, 
        External_Name => "png_write_chunk_end";

   function create_info_struct (png_ptr : png_const_structrp) return png_infop  -- /opt/homebrew/include/libpng16/png.h:1003
   with Import => True, 
        Convention => C, 
        External_Name => "png_create_info_struct";

   procedure info_init_3 (info_ptr : png_infopp; png_info_struct_size : size_t)  -- /opt/homebrew/include/libpng16/png.h:1010
   with Import => True, 
        Convention => C, 
        External_Name => "png_info_init_3";

   procedure write_info_before_PLTE (png_ptr : png_structrp; info_ptr : png_const_inforp)  -- /opt/homebrew/include/libpng16/png.h:1014
   with Import => True, 
        Convention => C, 
        External_Name => "png_write_info_before_PLTE";

   procedure write_info (png_ptr : png_structrp; info_ptr : png_const_inforp)  -- /opt/homebrew/include/libpng16/png.h:1016
   with Import => True, 
        Convention => C, 
        External_Name => "png_write_info";

   procedure read_info (png_ptr : png_structrp; info_ptr : png_inforp)  -- /opt/homebrew/include/libpng16/png.h:1021
   with Import => True, 
        Convention => C, 
        External_Name => "png_read_info";

   function convert_to_rfc1123 (png_ptr : png_structrp; ptime : png_const_timep) return adalibpng.conf.png_const_charp  -- /opt/homebrew/include/libpng16/png.h:1032
   with Import => True, 
        Convention => C, 
        External_Name => "png_convert_to_rfc1123";

   function convert_to_rfc1123_buffer (c_out : Interfaces.C.Strings.chars_ptr; ptime : png_const_timep) return int  -- /opt/homebrew/include/libpng16/png.h:1035
   with Import => True, 
        Convention => C, 
        External_Name => "png_convert_to_rfc1123_buffer";

   procedure convert_from_struct_tm (ptime : png_timep; ttime : access constant adalibpng.tm)  -- /opt/homebrew/include/libpng16/png.h:1041
   with Import => True, 
        Convention => C, 
        External_Name => "png_convert_from_struct_tm";

   procedure convert_from_time_t (ptime : png_timep; ttime : adalibpng.time_t)  -- /opt/homebrew/include/libpng16/png.h:1045
   with Import => True, 
        Convention => C, 
        External_Name => "png_convert_from_time_t";

   procedure set_expand (png_ptr : png_structrp)  -- /opt/homebrew/include/libpng16/png.h:1050
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_expand";

   procedure set_expand_gray_1_2_4_to_8 (png_ptr : png_structrp)  -- /opt/homebrew/include/libpng16/png.h:1051
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_expand_gray_1_2_4_to_8";

   procedure set_palette_to_rgb (png_ptr : png_structrp)  -- /opt/homebrew/include/libpng16/png.h:1052
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_palette_to_rgb";

   procedure set_tRNS_to_alpha (png_ptr : png_structrp)  -- /opt/homebrew/include/libpng16/png.h:1053
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_tRNS_to_alpha";

   procedure set_expand_16 (png_ptr : png_structrp)  -- /opt/homebrew/include/libpng16/png.h:1060
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_expand_16";

   procedure set_bgr (png_ptr : png_structrp)  -- /opt/homebrew/include/libpng16/png.h:1065
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_bgr";

   procedure set_gray_to_rgb (png_ptr : png_structrp)  -- /opt/homebrew/include/libpng16/png.h:1070
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_gray_to_rgb";

   procedure set_rgb_to_gray
     (png_ptr : png_structrp;
      error_action : int;
      red : double;
      green : double)  -- /opt/homebrew/include/libpng16/png.h:1080
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_rgb_to_gray";

   procedure set_rgb_to_gray_fixed
     (png_ptr : png_structrp;
      error_action : int;
      red : adalibpng.conf.png_fixed_point;
      green : adalibpng.conf.png_fixed_point)  -- /opt/homebrew/include/libpng16/png.h:1082
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_rgb_to_gray_fixed";

   function get_rgb_to_gray_status (png_ptr : png_const_structrp) return adalibpng.conf.png_byte  -- /opt/homebrew/include/libpng16/png.h:1085
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_rgb_to_gray_status";

   procedure build_grayscale_palette (bit_depth : int; palette : png_colorp)  -- /opt/homebrew/include/libpng16/png.h:1090
   with Import => True, 
        Convention => C, 
        External_Name => "png_build_grayscale_palette";

   procedure set_alpha_mode
     (png_ptr : png_structrp;
      mode : int;
      output_gamma : double)  -- /opt/homebrew/include/libpng16/png.h:1136
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_alpha_mode";

   procedure set_alpha_mode_fixed
     (png_ptr : png_structrp;
      mode : int;
      output_gamma : adalibpng.conf.png_fixed_point)  -- /opt/homebrew/include/libpng16/png.h:1138
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_alpha_mode_fixed";

   procedure set_strip_alpha (png_ptr : png_structrp)  -- /opt/homebrew/include/libpng16/png.h:1229
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_strip_alpha";

   procedure set_swap_alpha (png_ptr : png_structrp)  -- /opt/homebrew/include/libpng16/png.h:1234
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_swap_alpha";

   procedure set_invert_alpha (png_ptr : png_structrp)  -- /opt/homebrew/include/libpng16/png.h:1239
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_invert_alpha";

   procedure set_filler
     (png_ptr : png_structrp;
      filler : adalibpng.conf.png_uint_32;
      flags : int)  -- /opt/homebrew/include/libpng16/png.h:1244
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_filler";

   procedure set_add_alpha
     (png_ptr : png_structrp;
      filler : adalibpng.conf.png_uint_32;
      flags : int)  -- /opt/homebrew/include/libpng16/png.h:1250
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_add_alpha";

   procedure set_swap (png_ptr : png_structrp)  -- /opt/homebrew/include/libpng16/png.h:1256
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_swap";

   procedure set_packing (png_ptr : png_structrp)  -- /opt/homebrew/include/libpng16/png.h:1261
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_packing";

   procedure set_packswap (png_ptr : png_structrp)  -- /opt/homebrew/include/libpng16/png.h:1267
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_packswap";

   procedure set_shift (png_ptr : png_structrp; true_bits : png_const_color_8p)  -- /opt/homebrew/include/libpng16/png.h:1272
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_shift";

   function set_interlace_handling (png_ptr : png_structrp) return int  -- /opt/homebrew/include/libpng16/png.h:1284
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_interlace_handling";

   procedure set_invert_mono (png_ptr : png_structrp)  -- /opt/homebrew/include/libpng16/png.h:1289
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_invert_mono";

   procedure set_background
     (png_ptr : png_structrp;
      background_color : png_const_color_16p;
      background_gamma_code : int;
      need_expand : int;
      background_gamma : double)  -- /opt/homebrew/include/libpng16/png.h:1298
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_background";

   procedure set_background_fixed
     (png_ptr : png_structrp;
      background_color : png_const_color_16p;
      background_gamma_code : int;
      need_expand : int;
      background_gamma : adalibpng.conf.png_fixed_point)  -- /opt/homebrew/include/libpng16/png.h:1301
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_background_fixed";

   procedure set_scale_16 (png_ptr : png_structrp)  -- /opt/homebrew/include/libpng16/png.h:1314
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_scale_16";

   procedure set_strip_16 (png_ptr : png_structrp)  -- /opt/homebrew/include/libpng16/png.h:1320
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_strip_16";

   procedure set_quantize
     (png_ptr : png_structrp;
      palette : png_colorp;
      num_palette : int;
      maximum_colors : int;
      histogram : adalibpng.conf.png_const_uint_16p;
      full_quantize : int)  -- /opt/homebrew/include/libpng16/png.h:1327
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_quantize";

   procedure set_gamma
     (png_ptr : png_structrp;
      screen_gamma : double;
      override_file_gamma : double)  -- /opt/homebrew/include/libpng16/png.h:1349
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_gamma";

   procedure set_gamma_fixed
     (png_ptr : png_structrp;
      screen_gamma : adalibpng.conf.png_fixed_point;
      override_file_gamma : adalibpng.conf.png_fixed_point)  -- /opt/homebrew/include/libpng16/png.h:1351
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_gamma_fixed";

   procedure set_flush (png_ptr : png_structrp; nrows : int)  -- /opt/homebrew/include/libpng16/png.h:1357
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_flush";

   procedure write_flush (png_ptr : png_structrp)  -- /opt/homebrew/include/libpng16/png.h:1359
   with Import => True, 
        Convention => C, 
        External_Name => "png_write_flush";

   procedure start_read_image (png_ptr : png_structrp)  -- /opt/homebrew/include/libpng16/png.h:1363
   with Import => True, 
        Convention => C, 
        External_Name => "png_start_read_image";

   procedure read_update_info (png_ptr : png_structrp; info_ptr : png_inforp)  -- /opt/homebrew/include/libpng16/png.h:1366
   with Import => True, 
        Convention => C, 
        External_Name => "png_read_update_info";

   procedure read_rows
     (png_ptr : png_structrp;
      row : adalibpng.conf.png_bytepp;
      display_row : adalibpng.conf.png_bytepp;
      num_rows : adalibpng.conf.png_uint_32)  -- /opt/homebrew/include/libpng16/png.h:1371
   with Import => True, 
        Convention => C, 
        External_Name => "png_read_rows";

   procedure read_row
     (png_ptr : png_structrp;
      row : adalibpng.conf.png_bytep;
      display_row : adalibpng.conf.png_bytep)  -- /opt/homebrew/include/libpng16/png.h:1377
   with Import => True, 
        Convention => C, 
        External_Name => "png_read_row";

   procedure read_image (png_ptr : png_structrp; image : adalibpng.conf.png_bytepp)  -- /opt/homebrew/include/libpng16/png.h:1383
   with Import => True, 
        Convention => C, 
        External_Name => "png_read_image";

   procedure write_row (png_ptr : png_structrp; row : adalibpng.conf.png_const_bytep)  -- /opt/homebrew/include/libpng16/png.h:1387
   with Import => True, 
        Convention => C, 
        External_Name => "png_write_row";

   procedure write_rows
     (png_ptr : png_structrp;
      row : adalibpng.conf.png_bytepp;
      num_rows : adalibpng.conf.png_uint_32)  -- /opt/homebrew/include/libpng16/png.h:1395
   with Import => True, 
        Convention => C, 
        External_Name => "png_write_rows";

   procedure write_image (png_ptr : png_structrp; image : adalibpng.conf.png_bytepp)  -- /opt/homebrew/include/libpng16/png.h:1399
   with Import => True, 
        Convention => C, 
        External_Name => "png_write_image";

   procedure write_end (png_ptr : png_structrp; info_ptr : png_inforp)  -- /opt/homebrew/include/libpng16/png.h:1402
   with Import => True, 
        Convention => C, 
        External_Name => "png_write_end";

   procedure read_end (png_ptr : png_structrp; info_ptr : png_inforp)  -- /opt/homebrew/include/libpng16/png.h:1407
   with Import => True, 
        Convention => C, 
        External_Name => "png_read_end";

   procedure destroy_info_struct (png_ptr : png_const_structrp; info_ptr_ptr : png_infopp)  -- /opt/homebrew/include/libpng16/png.h:1411
   with Import => True, 
        Convention => C, 
        External_Name => "png_destroy_info_struct";

   procedure destroy_read_struct
     (png_ptr_ptr : png_structpp;
      info_ptr_ptr : png_infopp;
      end_info_ptr_ptr : png_infopp)  -- /opt/homebrew/include/libpng16/png.h:1415
   with Import => True, 
        Convention => C, 
        External_Name => "png_destroy_read_struct";

   procedure destroy_write_struct (png_ptr_ptr : png_structpp; info_ptr_ptr : png_infopp)  -- /opt/homebrew/include/libpng16/png.h:1419
   with Import => True, 
        Convention => C, 
        External_Name => "png_destroy_write_struct";

   procedure set_crc_action
     (png_ptr : png_structrp;
      crit_action : int;
      ancil_action : int)  -- /opt/homebrew/include/libpng16/png.h:1423
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_crc_action";

   procedure set_filter
     (png_ptr : png_structrp;
      method : int;
      filters : int)  -- /opt/homebrew/include/libpng16/png.h:1454
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_filter";

   procedure set_filter_heuristics
     (png_ptr : png_structrp;
      heuristic_method : int;
      num_weights : int;
      filter_weights : adalibpng.conf.png_const_doublep;
      filter_costs : adalibpng.conf.png_const_doublep)  -- /opt/homebrew/include/libpng16/png.h:1484
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_filter_heuristics";

   procedure set_filter_heuristics_fixed
     (png_ptr : png_structrp;
      heuristic_method : int;
      num_weights : int;
      filter_weights : adalibpng.conf.png_const_fixed_point_p;
      filter_costs : adalibpng.conf.png_const_fixed_point_p)  -- /opt/homebrew/include/libpng16/png.h:1487
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_filter_heuristics_fixed";

   procedure set_compression_level (png_ptr : png_structrp; level : int)  -- /opt/homebrew/include/libpng16/png.h:1507
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_compression_level";

   procedure set_compression_mem_level (png_ptr : png_structrp; mem_level : int)  -- /opt/homebrew/include/libpng16/png.h:1510
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_compression_mem_level";

   procedure set_compression_strategy (png_ptr : png_structrp; strategy : int)  -- /opt/homebrew/include/libpng16/png.h:1513
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_compression_strategy";

   procedure set_compression_window_bits (png_ptr : png_structrp; window_bits : int)  -- /opt/homebrew/include/libpng16/png.h:1519
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_compression_window_bits";

   procedure set_compression_method (png_ptr : png_structrp; method : int)  -- /opt/homebrew/include/libpng16/png.h:1522
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_compression_method";

   procedure set_text_compression_level (png_ptr : png_structrp; level : int)  -- /opt/homebrew/include/libpng16/png.h:1528
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_text_compression_level";

   procedure set_text_compression_mem_level (png_ptr : png_structrp; mem_level : int)  -- /opt/homebrew/include/libpng16/png.h:1531
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_text_compression_mem_level";

   procedure set_text_compression_strategy (png_ptr : png_structrp; strategy : int)  -- /opt/homebrew/include/libpng16/png.h:1534
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_text_compression_strategy";

   procedure set_text_compression_window_bits (png_ptr : png_structrp; window_bits : int)  -- /opt/homebrew/include/libpng16/png.h:1540
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_text_compression_window_bits";

   procedure set_text_compression_method (png_ptr : png_structrp; method : int)  -- /opt/homebrew/include/libpng16/png.h:1543
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_text_compression_method";

   procedure init_io (png_ptr : png_structrp; fp : adalibpng.ICS.FILEs)  -- /opt/homebrew/include/libpng16/png.h:1559
   with Import => True, 
        Convention => C, 
        External_Name => "png_init_io";

   procedure set_error_fn
     (png_ptr : png_structrp;
      error_ptr : adalibpng.conf.png_voidp;
      error_fn : png_error_ptr;
      warning_fn : png_error_ptr)  -- /opt/homebrew/include/libpng16/png.h:1570
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_error_fn";

   function get_error_ptr (png_ptr : png_const_structrp) return adalibpng.conf.png_voidp  -- /opt/homebrew/include/libpng16/png.h:1574
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_error_ptr";

   procedure set_write_fn
     (png_ptr : png_structrp;
      io_ptr : adalibpng.conf.png_voidp;
      write_data_fn : png_rw_ptr;
      output_flush_fn : png_flush_ptr)  -- /opt/homebrew/include/libpng16/png.h:1586
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_write_fn";

   procedure set_read_fn
     (png_ptr : png_structrp;
      io_ptr : adalibpng.conf.png_voidp;
      read_data_fn : png_rw_ptr)  -- /opt/homebrew/include/libpng16/png.h:1590
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_read_fn";

   function get_io_ptr (png_ptr : png_const_structrp) return adalibpng.conf.png_voidp  -- /opt/homebrew/include/libpng16/png.h:1594
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_io_ptr";

   procedure set_read_status_fn (png_ptr : png_structrp; read_row_fn : png_read_status_ptr)  -- /opt/homebrew/include/libpng16/png.h:1596
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_read_status_fn";

   procedure set_write_status_fn (png_ptr : png_structrp; write_row_fn : png_write_status_ptr)  -- /opt/homebrew/include/libpng16/png.h:1599
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_write_status_fn";

   procedure set_mem_fn
     (png_ptr : png_structrp;
      mem_ptr : adalibpng.conf.png_voidp;
      malloc_fn : png_malloc_ptr;
      free_fn : png_free_ptr)  -- /opt/homebrew/include/libpng16/png.h:1604
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_mem_fn";

   function get_mem_ptr (png_ptr : png_const_structrp) return adalibpng.conf.png_voidp  -- /opt/homebrew/include/libpng16/png.h:1607
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_mem_ptr";

   procedure set_read_user_transform_fn (png_ptr : png_structrp; read_user_transform_fn : png_user_transform_ptr)  -- /opt/homebrew/include/libpng16/png.h:1611
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_read_user_transform_fn";

   procedure set_write_user_transform_fn (png_ptr : png_structrp; write_user_transform_fn : png_user_transform_ptr)  -- /opt/homebrew/include/libpng16/png.h:1616
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_write_user_transform_fn";

   procedure set_user_transform_info
     (png_ptr : png_structrp;
      user_transform_ptr : adalibpng.conf.png_voidp;
      user_transform_depth : int;
      user_transform_channels : int)  -- /opt/homebrew/include/libpng16/png.h:1621
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_user_transform_info";

   function get_user_transform_ptr (png_ptr : png_const_structrp) return adalibpng.conf.png_voidp  -- /opt/homebrew/include/libpng16/png.h:1625
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_user_transform_ptr";

   function get_current_row_number (arg1 : png_const_structrp) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:1641
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_current_row_number";

   function get_current_pass_number (arg1 : png_const_structrp) return adalibpng.conf.png_byte  -- /opt/homebrew/include/libpng16/png.h:1642
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_current_pass_number";

   procedure set_read_user_chunk_fn
     (png_ptr : png_structrp;
      user_chunk_ptr : adalibpng.conf.png_voidp;
      read_user_chunk_fn : png_user_chunk_ptr)  -- /opt/homebrew/include/libpng16/png.h:1665
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_read_user_chunk_fn";

   function get_user_chunk_ptr (png_ptr : png_const_structrp) return adalibpng.conf.png_voidp  -- /opt/homebrew/include/libpng16/png.h:1670
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_user_chunk_ptr";

   procedure set_progressive_read_fn
     (png_ptr : png_structrp;
      progressive_ptr : adalibpng.conf.png_voidp;
      info_fn : png_progressive_info_ptr;
      row_fn : png_progressive_row_ptr;
      end_fn : png_progressive_end_ptr)  -- /opt/homebrew/include/libpng16/png.h:1677
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_progressive_read_fn";

   function get_progressive_ptr (png_ptr : png_const_structrp) return adalibpng.conf.png_voidp  -- /opt/homebrew/include/libpng16/png.h:1682
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_progressive_ptr";

   procedure process_data
     (png_ptr : png_structrp;
      info_ptr : png_inforp;
      buffer : adalibpng.conf.png_bytep;
      buffer_size : size_t)  -- /opt/homebrew/include/libpng16/png.h:1686
   with Import => True, 
        Convention => C, 
        External_Name => "png_process_data";

   function process_data_pause (arg1 : png_structrp; save : int) return size_t  -- /opt/homebrew/include/libpng16/png.h:1696
   with Import => True, 
        Convention => C, 
        External_Name => "png_process_data_pause";

   function process_data_skip (arg1 : png_structrp) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:1704
   with Import => True, 
        Convention => C, 
        External_Name => "png_process_data_skip";

   procedure progressive_combine_row
     (png_ptr : png_const_structrp;
      old_row : adalibpng.conf.png_bytep;
      new_row : adalibpng.conf.png_const_bytep)  -- /opt/homebrew/include/libpng16/png.h:1711
   with Import => True, 
        Convention => C, 
        External_Name => "png_progressive_combine_row";

   function malloc (png_ptr : png_const_structrp; size : adalibpng.conf.png_alloc_size_t) return adalibpng.conf.png_voidp  -- /opt/homebrew/include/libpng16/png.h:1715
   with Import => True, 
        Convention => C, 
        External_Name => "png_malloc";

   function calloc (png_ptr : png_const_structrp; size : adalibpng.conf.png_alloc_size_t) return adalibpng.conf.png_voidp  -- /opt/homebrew/include/libpng16/png.h:1718
   with Import => True, 
        Convention => C, 
        External_Name => "png_calloc";

   function malloc_warn (png_ptr : png_const_structrp; size : adalibpng.conf.png_alloc_size_t) return adalibpng.conf.png_voidp  -- /opt/homebrew/include/libpng16/png.h:1722
   with Import => True, 
        Convention => C, 
        External_Name => "png_malloc_warn";

   procedure free (png_ptr : png_const_structrp; ptr : adalibpng.conf.png_voidp)  -- /opt/homebrew/include/libpng16/png.h:1726
   with Import => True, 
        Convention => C, 
        External_Name => "png_free";

   procedure free_data
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      free_me : adalibpng.conf.png_uint_32;
      num : int)  -- /opt/homebrew/include/libpng16/png.h:1729
   with Import => True, 
        Convention => C, 
        External_Name => "png_free_data";

   procedure data_freer
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      freer : int;
      mask : adalibpng.conf.png_uint_32)  -- /opt/homebrew/include/libpng16/png.h:1736
   with Import => True, 
        Convention => C, 
        External_Name => "png_data_freer";

   function malloc_default (png_ptr : png_const_structrp; size : adalibpng.conf.png_alloc_size_t) return adalibpng.conf.png_voidp  -- /opt/homebrew/include/libpng16/png.h:1762
   with Import => True, 
        Convention => C, 
        External_Name => "png_malloc_default";

   procedure free_default (png_ptr : png_const_structrp; ptr : adalibpng.conf.png_voidp)  -- /opt/homebrew/include/libpng16/png.h:1764
   with Import => True, 
        Convention => C, 
        External_Name => "png_free_default";

   procedure error (png_ptr : png_const_structrp; error_message : adalibpng.conf.png_const_charp)  -- /opt/homebrew/include/libpng16/png.h:1770
   with Import => True, 
        Convention => C, 
        External_Name => "png_error";

   procedure chunk_error (png_ptr : png_const_structrp; error_message : adalibpng.conf.png_const_charp)  -- /opt/homebrew/include/libpng16/png.h:1774
   with Import => True, 
        Convention => C, 
        External_Name => "png_chunk_error";

   procedure warning (png_ptr : png_const_structrp; warning_message : adalibpng.conf.png_const_charp)  -- /opt/homebrew/include/libpng16/png.h:1786
   with Import => True, 
        Convention => C, 
        External_Name => "png_warning";

   procedure chunk_warning (png_ptr : png_const_structrp; warning_message : adalibpng.conf.png_const_charp)  -- /opt/homebrew/include/libpng16/png.h:1790
   with Import => True, 
        Convention => C, 
        External_Name => "png_chunk_warning";

   procedure benign_error (png_ptr : png_const_structrp; warning_message : adalibpng.conf.png_const_charp)  -- /opt/homebrew/include/libpng16/png.h:1800
   with Import => True, 
        Convention => C, 
        External_Name => "png_benign_error";

   procedure chunk_benign_error (png_ptr : png_const_structrp; warning_message : adalibpng.conf.png_const_charp)  -- /opt/homebrew/include/libpng16/png.h:1805
   with Import => True, 
        Convention => C, 
        External_Name => "png_chunk_benign_error";

   procedure set_benign_errors (png_ptr : png_structrp; allowed : int)  -- /opt/homebrew/include/libpng16/png.h:1809
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_benign_errors";

   function get_valid
     (png_ptr : png_const_structrp;
      info_ptr : png_const_inforp;
      flag : adalibpng.conf.png_uint_32) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:1834
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_valid";

   function get_rowbytes (png_ptr : png_const_structrp; info_ptr : png_const_inforp) return size_t  -- /opt/homebrew/include/libpng16/png.h:1838
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_rowbytes";

   function get_rows (png_ptr : png_const_structrp; info_ptr : png_const_inforp) return adalibpng.conf.png_bytepp  -- /opt/homebrew/include/libpng16/png.h:1845
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_rows";

   procedure set_rows
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      row_pointers : adalibpng.conf.png_bytepp)  -- /opt/homebrew/include/libpng16/png.h:1851
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_rows";

   function get_channels (png_ptr : png_const_structrp; info_ptr : png_const_inforp) return adalibpng.conf.png_byte  -- /opt/homebrew/include/libpng16/png.h:1856
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_channels";

   function get_image_width (png_ptr : png_const_structrp; info_ptr : png_const_inforp) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:1861
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_image_width";

   function get_image_height (png_ptr : png_const_structrp; info_ptr : png_const_inforp) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:1865
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_image_height";

   function get_bit_depth (png_ptr : png_const_structrp; info_ptr : png_const_inforp) return adalibpng.conf.png_byte  -- /opt/homebrew/include/libpng16/png.h:1869
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_bit_depth";

   function get_color_type (png_ptr : png_const_structrp; info_ptr : png_const_inforp) return adalibpng.conf.png_byte  -- /opt/homebrew/include/libpng16/png.h:1873
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_color_type";

   function get_filter_type (png_ptr : png_const_structrp; info_ptr : png_const_inforp) return adalibpng.conf.png_byte  -- /opt/homebrew/include/libpng16/png.h:1877
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_filter_type";

   function get_interlace_type (png_ptr : png_const_structrp; info_ptr : png_const_inforp) return adalibpng.conf.png_byte  -- /opt/homebrew/include/libpng16/png.h:1881
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_interlace_type";

   function get_compression_type (png_ptr : png_const_structrp; info_ptr : png_const_inforp) return adalibpng.conf.png_byte  -- /opt/homebrew/include/libpng16/png.h:1885
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_compression_type";

   function get_pixels_per_meter (png_ptr : png_const_structrp; info_ptr : png_const_inforp) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:1889
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_pixels_per_meter";

   function get_x_pixels_per_meter (png_ptr : png_const_structrp; info_ptr : png_const_inforp) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:1891
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_x_pixels_per_meter";

   function get_y_pixels_per_meter (png_ptr : png_const_structrp; info_ptr : png_const_inforp) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:1893
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_y_pixels_per_meter";

   function get_pixel_aspect_ratio (png_ptr : png_const_structrp; info_ptr : png_const_inforp) return float  -- /opt/homebrew/include/libpng16/png.h:1897
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_pixel_aspect_ratio";

   function get_pixel_aspect_ratio_fixed (png_ptr : png_const_structrp; info_ptr : png_const_inforp) return adalibpng.conf.png_fixed_point  -- /opt/homebrew/include/libpng16/png.h:1899
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_pixel_aspect_ratio_fixed";

   function get_x_offset_pixels (png_ptr : png_const_structrp; info_ptr : png_const_inforp) return adalibpng.conf.png_int_32  -- /opt/homebrew/include/libpng16/png.h:1903
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_x_offset_pixels";

   function get_y_offset_pixels (png_ptr : png_const_structrp; info_ptr : png_const_inforp) return adalibpng.conf.png_int_32  -- /opt/homebrew/include/libpng16/png.h:1905
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_y_offset_pixels";

   function get_x_offset_microns (png_ptr : png_const_structrp; info_ptr : png_const_inforp) return adalibpng.conf.png_int_32  -- /opt/homebrew/include/libpng16/png.h:1907
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_x_offset_microns";

   function get_y_offset_microns (png_ptr : png_const_structrp; info_ptr : png_const_inforp) return adalibpng.conf.png_int_32  -- /opt/homebrew/include/libpng16/png.h:1909
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_y_offset_microns";

   function get_signature (png_ptr : png_const_structrp; info_ptr : png_const_inforp) return adalibpng.conf.png_const_bytep  -- /opt/homebrew/include/libpng16/png.h:1916
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_signature";

   function get_bKGD
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      background : System.Address) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:1921
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_bKGD";

   procedure set_bKGD
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      background : png_const_color_16p)  -- /opt/homebrew/include/libpng16/png.h:1926
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_bKGD";

   function get_cHRM
     (png_ptr : png_const_structrp;
      info_ptr : png_const_inforp;
      white_x : access double;
      white_y : access double;
      red_x : access double;
      red_y : access double;
      green_x : access double;
      green_y : access double;
      blue_x : access double;
      blue_y : access double) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:1931
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_cHRM";

   function get_cHRM_XYZ
     (png_ptr : png_const_structrp;
      info_ptr : png_const_inforp;
      red_X : access double;
      red_Y : access double;
      red_Z : access double;
      green_X : access double;
      green_Y : access double;
      green_Z : access double;
      blue_X : access double;
      blue_Y : access double;
      blue_Z : access double) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:1935
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_cHRM_XYZ";

   function get_cHRM_fixed
     (png_ptr : png_const_structrp;
      info_ptr : png_const_inforp;
      int_white_x : access adalibpng.conf.png_fixed_point;
      int_white_y : access adalibpng.conf.png_fixed_point;
      int_red_x : access adalibpng.conf.png_fixed_point;
      int_red_y : access adalibpng.conf.png_fixed_point;
      int_green_x : access adalibpng.conf.png_fixed_point;
      int_green_y : access adalibpng.conf.png_fixed_point;
      int_blue_x : access adalibpng.conf.png_fixed_point;
      int_blue_y : access adalibpng.conf.png_fixed_point) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:1939
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_cHRM_fixed";

   function get_cHRM_XYZ_fixed
     (png_ptr : png_const_structrp;
      info_ptr : png_const_inforp;
      int_red_X : access adalibpng.conf.png_fixed_point;
      int_red_Y : access adalibpng.conf.png_fixed_point;
      int_red_Z : access adalibpng.conf.png_fixed_point;
      int_green_X : access adalibpng.conf.png_fixed_point;
      int_green_Y : access adalibpng.conf.png_fixed_point;
      int_green_Z : access adalibpng.conf.png_fixed_point;
      int_blue_X : access adalibpng.conf.png_fixed_point;
      int_blue_Y : access adalibpng.conf.png_fixed_point;
      int_blue_Z : access adalibpng.conf.png_fixed_point) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:1945
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_cHRM_XYZ_fixed";

   procedure set_cHRM
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      white_x : double;
      white_y : double;
      red_x : double;
      red_y : double;
      green_x : double;
      green_y : double;
      blue_x : double;
      blue_y : double)  -- /opt/homebrew/include/libpng16/png.h:1955
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_cHRM";

   procedure set_cHRM_XYZ
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      red_X : double;
      red_Y : double;
      red_Z : double;
      green_X : double;
      green_Y : double;
      green_Z : double;
      blue_X : double;
      blue_Y : double;
      blue_Z : double)  -- /opt/homebrew/include/libpng16/png.h:1959
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_cHRM_XYZ";

   procedure set_cHRM_fixed
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      int_white_x : adalibpng.conf.png_fixed_point;
      int_white_y : adalibpng.conf.png_fixed_point;
      int_red_x : adalibpng.conf.png_fixed_point;
      int_red_y : adalibpng.conf.png_fixed_point;
      int_green_x : adalibpng.conf.png_fixed_point;
      int_green_y : adalibpng.conf.png_fixed_point;
      int_blue_x : adalibpng.conf.png_fixed_point;
      int_blue_y : adalibpng.conf.png_fixed_point)  -- /opt/homebrew/include/libpng16/png.h:1963
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_cHRM_fixed";

   procedure set_cHRM_XYZ_fixed
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      int_red_X : adalibpng.conf.png_fixed_point;
      int_red_Y : adalibpng.conf.png_fixed_point;
      int_red_Z : adalibpng.conf.png_fixed_point;
      int_green_X : adalibpng.conf.png_fixed_point;
      int_green_Y : adalibpng.conf.png_fixed_point;
      int_green_Z : adalibpng.conf.png_fixed_point;
      int_blue_X : adalibpng.conf.png_fixed_point;
      int_blue_Y : adalibpng.conf.png_fixed_point;
      int_blue_Z : adalibpng.conf.png_fixed_point)  -- /opt/homebrew/include/libpng16/png.h:1969
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_cHRM_XYZ_fixed";

   function get_eXIf
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      exif : System.Address) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:1978
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_eXIf";

   procedure set_eXIf
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      exif : adalibpng.conf.png_bytep)  -- /opt/homebrew/include/libpng16/png.h:1980
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_eXIf";

   function get_eXIf_1
     (png_ptr : png_const_structrp;
      info_ptr : png_const_inforp;
      num_exif : access adalibpng.conf.png_uint_32;
      exif : System.Address) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:1983
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_eXIf_1";

   procedure set_eXIf_1
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      num_exif : adalibpng.conf.png_uint_32;
      exif : adalibpng.conf.png_bytep)  -- /opt/homebrew/include/libpng16/png.h:1985
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_eXIf_1";

   function get_gAMA
     (png_ptr : png_const_structrp;
      info_ptr : png_const_inforp;
      file_gamma : access double) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:1990
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_gAMA";

   function get_gAMA_fixed
     (png_ptr : png_const_structrp;
      info_ptr : png_const_inforp;
      int_file_gamma : access adalibpng.conf.png_fixed_point) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:1992
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_gAMA_fixed";

   procedure set_gAMA
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      file_gamma : double)  -- /opt/homebrew/include/libpng16/png.h:1998
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_gAMA";

   procedure set_gAMA_fixed
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      int_file_gamma : adalibpng.conf.png_fixed_point)  -- /opt/homebrew/include/libpng16/png.h:2000
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_gAMA_fixed";

   function get_hIST
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      hist : System.Address) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:2005
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_hIST";

   procedure set_hIST
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      hist : adalibpng.conf.png_const_uint_16p)  -- /opt/homebrew/include/libpng16/png.h:2007
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_hIST";

   function get_IHDR
     (png_ptr : png_const_structrp;
      info_ptr : png_const_inforp;
      width : access adalibpng.conf.png_uint_32;
      height : access adalibpng.conf.png_uint_32;
      bit_depth : access int;
      color_type : access int;
      interlace_method : access int;
      compression_method : access int;
      filter_method : access int) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:2011
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_IHDR";

   procedure set_IHDR
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      width : adalibpng.conf.png_uint_32;
      height : adalibpng.conf.png_uint_32;
      bit_depth : int;
      color_type : int;
      interlace_method : int;
      compression_method : int;
      filter_method : int)  -- /opt/homebrew/include/libpng16/png.h:2016
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_IHDR";

   function get_oFFs
     (png_ptr : png_const_structrp;
      info_ptr : png_const_inforp;
      offset_x : access adalibpng.conf.png_int_32;
      offset_y : access adalibpng.conf.png_int_32;
      unit_type : access int) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:2022
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_oFFs";

   procedure set_oFFs
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      offset_x : adalibpng.conf.png_int_32;
      offset_y : adalibpng.conf.png_int_32;
      unit_type : int)  -- /opt/homebrew/include/libpng16/png.h:2028
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_oFFs";

   function get_pCAL
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      purpose : System.Address;
      X0 : access adalibpng.conf.png_int_32;
      X1 : access adalibpng.conf.png_int_32;
      c_type : access int;
      nparams : access int;
      units : System.Address;
      params : System.Address) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:2034
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_pCAL";

   procedure set_pCAL
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      purpose : adalibpng.conf.png_const_charp;
      X0 : adalibpng.conf.png_int_32;
      X1 : adalibpng.conf.png_int_32;
      c_type : int;
      nparams : int;
      units : adalibpng.conf.png_const_charp;
      params : adalibpng.conf.png_charpp)  -- /opt/homebrew/include/libpng16/png.h:2041
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_pCAL";

   function get_pHYs
     (png_ptr : png_const_structrp;
      info_ptr : png_const_inforp;
      res_x : access adalibpng.conf.png_uint_32;
      res_y : access adalibpng.conf.png_uint_32;
      unit_type : access int) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:2047
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_pHYs";

   procedure set_pHYs
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      res_x : adalibpng.conf.png_uint_32;
      res_y : adalibpng.conf.png_uint_32;
      unit_type : int)  -- /opt/homebrew/include/libpng16/png.h:2053
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_pHYs";

   function get_PLTE
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      palette : System.Address;
      num_palette : access int) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:2057
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_PLTE";

   procedure set_PLTE
     (png_ptr : png_structrp;
      info_ptr : png_inforp;
      palette : png_const_colorp;
      num_palette : int)  -- /opt/homebrew/include/libpng16/png.h:2060
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_PLTE";

   function get_sBIT
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      sig_bit : System.Address) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:2064
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_sBIT";

   procedure set_sBIT
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      sig_bit : png_const_color_8p)  -- /opt/homebrew/include/libpng16/png.h:2069
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_sBIT";

   function get_sRGB
     (png_ptr : png_const_structrp;
      info_ptr : png_const_inforp;
      file_srgb_intent : access int) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:2074
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_sRGB";

   procedure set_sRGB
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      srgb_intent : int)  -- /opt/homebrew/include/libpng16/png.h:2079
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_sRGB";

   procedure set_sRGB_gAMA_and_cHRM
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      srgb_intent : int)  -- /opt/homebrew/include/libpng16/png.h:2081
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_sRGB_gAMA_and_cHRM";

   function get_iCCP
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      name : adalibpng.conf.png_charpp;
      compression_type : access int;
      profile : adalibpng.conf.png_bytepp;
      proflen : access adalibpng.conf.png_uint_32) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:2086
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_iCCP";

   procedure set_iCCP
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      name : adalibpng.conf.png_const_charp;
      compression_type : int;
      profile : adalibpng.conf.png_const_bytep;
      proflen : adalibpng.conf.png_uint_32)  -- /opt/homebrew/include/libpng16/png.h:2092
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_iCCP";

   function get_sPLT
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      entries : png_sPLT_tpp) return int  -- /opt/homebrew/include/libpng16/png.h:2098
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_sPLT";

   procedure set_sPLT
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      entries : png_const_sPLT_tp;
      nentries : int)  -- /opt/homebrew/include/libpng16/png.h:2103
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_sPLT";

   function get_text
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      text_ptr : System.Address;
      num_text : access int) return int  -- /opt/homebrew/include/libpng16/png.h:2109
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_text";

   procedure set_text
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      text_ptr : png_const_textp;
      num_text : int)  -- /opt/homebrew/include/libpng16/png.h:2121
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_text";

   function get_tIME
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      mod_time : System.Address) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:2126
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_tIME";

   procedure set_tIME
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      mod_time : png_const_timep)  -- /opt/homebrew/include/libpng16/png.h:2131
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_tIME";

   function get_tRNS
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      trans_alpha : System.Address;
      num_trans : access int;
      trans_color : System.Address) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:2136
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_tRNS";

   procedure set_tRNS
     (png_ptr : png_structrp;
      info_ptr : png_inforp;
      trans_alpha : adalibpng.conf.png_const_bytep;
      num_trans : int;
      trans_color : png_const_color_16p)  -- /opt/homebrew/include/libpng16/png.h:2142
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_tRNS";

   function get_sCAL
     (png_ptr : png_const_structrp;
      info_ptr : png_const_inforp;
      unit : access int;
      width : access double;
      height : access double) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:2148
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_sCAL";

   function get_sCAL_fixed
     (png_ptr : png_const_structrp;
      info_ptr : png_const_inforp;
      unit : access int;
      width : access adalibpng.conf.png_fixed_point;
      height : access adalibpng.conf.png_fixed_point) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:2157
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_sCAL_fixed";

   function get_sCAL_s
     (png_ptr : png_const_structrp;
      info_ptr : png_const_inforp;
      unit : access int;
      swidth : adalibpng.conf.png_charpp;
      sheight : adalibpng.conf.png_charpp) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:2161
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_sCAL_s";

   procedure set_sCAL
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      unit : int;
      width : double;
      height : double)  -- /opt/homebrew/include/libpng16/png.h:2165
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_sCAL";

   procedure set_sCAL_fixed
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      unit : int;
      width : adalibpng.conf.png_fixed_point;
      height : adalibpng.conf.png_fixed_point)  -- /opt/homebrew/include/libpng16/png.h:2167
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_sCAL_fixed";

   procedure set_sCAL_s
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      unit : int;
      swidth : adalibpng.conf.png_const_charp;
      sheight : adalibpng.conf.png_const_charp)  -- /opt/homebrew/include/libpng16/png.h:2170
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_sCAL_s";

   procedure set_keep_unknown_chunks
     (png_ptr : png_structrp;
      keep : int;
      chunk_list : adalibpng.conf.png_const_bytep;
      num_chunks : int)  -- /opt/homebrew/include/libpng16/png.h:2275
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_keep_unknown_chunks";

   function handle_as_unknown (png_ptr : png_const_structrp; chunk_name : adalibpng.conf.png_const_bytep) return int  -- /opt/homebrew/include/libpng16/png.h:2283
   with Import => True, 
        Convention => C, 
        External_Name => "png_handle_as_unknown";

   procedure set_unknown_chunks
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      unknowns : png_const_unknown_chunkp;
      num_unknowns : int)  -- /opt/homebrew/include/libpng16/png.h:2288
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_unknown_chunks";

   procedure set_unknown_chunk_location
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      chunk : int;
      location : int)  -- /opt/homebrew/include/libpng16/png.h:2300
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_unknown_chunk_location";

   function get_unknown_chunks
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      entries : png_unknown_chunkpp) return int  -- /opt/homebrew/include/libpng16/png.h:2303
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_unknown_chunks";

   procedure set_invalid
     (png_ptr : png_const_structrp;
      info_ptr : png_inforp;
      mask : int)  -- /opt/homebrew/include/libpng16/png.h:2311
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_invalid";

   procedure read_png
     (png_ptr : png_structrp;
      info_ptr : png_inforp;
      transforms : int;
      params : adalibpng.conf.png_voidp)  -- /opt/homebrew/include/libpng16/png.h:2317
   with Import => True, 
        Convention => C, 
        External_Name => "png_read_png";

   procedure write_png
     (png_ptr : png_structrp;
      info_ptr : png_inforp;
      transforms : int;
      params : adalibpng.conf.png_voidp)  -- /opt/homebrew/include/libpng16/png.h:2321
   with Import => True, 
        Convention => C, 
        External_Name => "png_write_png";

   function get_copyright (png_ptr : png_const_structrp) return adalibpng.conf.png_const_charp  -- /opt/homebrew/include/libpng16/png.h:2326
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_copyright";

   function get_header_ver (png_ptr : png_const_structrp) return adalibpng.conf.png_const_charp  -- /opt/homebrew/include/libpng16/png.h:2328
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_header_ver";

   function get_header_version (png_ptr : png_const_structrp) return adalibpng.conf.png_const_charp  -- /opt/homebrew/include/libpng16/png.h:2330
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_header_version";

   function get_libpng_ver (png_ptr : png_const_structrp) return adalibpng.conf.png_const_charp  -- /opt/homebrew/include/libpng16/png.h:2332
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_libpng_ver";

   function permit_mng_features (png_ptr : png_structrp; mng_features_permitted : adalibpng.conf.png_uint_32) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:2336
   with Import => True, 
        Convention => C, 
        External_Name => "png_permit_mng_features";

   procedure set_user_limits
     (png_ptr : png_structrp;
      user_width_max : adalibpng.conf.png_uint_32;
      user_height_max : adalibpng.conf.png_uint_32)  -- /opt/homebrew/include/libpng16/png.h:2357
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_user_limits";

   function get_user_width_max (png_ptr : png_const_structrp) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:2359
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_user_width_max";

   function get_user_height_max (png_ptr : png_const_structrp) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:2361
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_user_height_max";

   procedure set_chunk_cache_max (png_ptr : png_structrp; user_chunk_cache_max : adalibpng.conf.png_uint_32)  -- /opt/homebrew/include/libpng16/png.h:2364
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_chunk_cache_max";

   function get_chunk_cache_max (png_ptr : png_const_structrp) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:2366
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_chunk_cache_max";

   procedure set_chunk_malloc_max (png_ptr : png_structrp; user_chunk_cache_max : adalibpng.conf.png_alloc_size_t)  -- /opt/homebrew/include/libpng16/png.h:2369
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_chunk_malloc_max";

   function get_chunk_malloc_max (png_ptr : png_const_structrp) return adalibpng.conf.png_alloc_size_t  -- /opt/homebrew/include/libpng16/png.h:2371
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_chunk_malloc_max";

   function get_pixels_per_inch (png_ptr : png_const_structrp; info_ptr : png_const_inforp) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:2376
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_pixels_per_inch";

   function get_x_pixels_per_inch (png_ptr : png_const_structrp; info_ptr : png_const_inforp) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:2379
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_x_pixels_per_inch";

   function get_y_pixels_per_inch (png_ptr : png_const_structrp; info_ptr : png_const_inforp) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:2382
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_y_pixels_per_inch";

   function get_x_offset_inches (png_ptr : png_const_structrp; info_ptr : png_const_inforp) return float  -- /opt/homebrew/include/libpng16/png.h:2385
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_x_offset_inches";

   function get_x_offset_inches_fixed (png_ptr : png_const_structrp; info_ptr : png_const_inforp) return adalibpng.conf.png_fixed_point  -- /opt/homebrew/include/libpng16/png.h:2388
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_x_offset_inches_fixed";

   function get_y_offset_inches (png_ptr : png_const_structrp; info_ptr : png_const_inforp) return float  -- /opt/homebrew/include/libpng16/png.h:2392
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_y_offset_inches";

   function get_y_offset_inches_fixed (png_ptr : png_const_structrp; info_ptr : png_const_inforp) return adalibpng.conf.png_fixed_point  -- /opt/homebrew/include/libpng16/png.h:2395
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_y_offset_inches_fixed";

   function get_pHYs_dpi
     (png_ptr : png_const_structrp;
      info_ptr : png_const_inforp;
      res_x : access adalibpng.conf.png_uint_32;
      res_y : access adalibpng.conf.png_uint_32;
      unit_type : access int) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:2400
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_pHYs_dpi";

   function get_io_state (png_ptr : png_const_structrp) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:2408
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_io_state";

   function get_io_chunk_type (png_ptr : png_const_structrp) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:2414
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_io_chunk_type";

   function get_uint_32 (buf : adalibpng.conf.png_const_bytep) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:2540
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_uint_32";

   function get_uint_16 (buf : adalibpng.conf.png_const_bytep) return adalibpng.conf.png_uint_16  -- /opt/homebrew/include/libpng16/png.h:2541
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_uint_16";

   function get_int_32 (buf : adalibpng.conf.png_const_bytep) return adalibpng.conf.png_int_32  -- /opt/homebrew/include/libpng16/png.h:2542
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_int_32";

   function get_uint_31 (png_ptr : png_const_structrp; buf : adalibpng.conf.png_const_bytep) return adalibpng.conf.png_uint_32  -- /opt/homebrew/include/libpng16/png.h:2545
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_uint_31";

   procedure save_uint_32 (buf : adalibpng.conf.png_bytep; i : adalibpng.conf.png_uint_32)  -- /opt/homebrew/include/libpng16/png.h:2551
   with Import => True, 
        Convention => C, 
        External_Name => "png_save_uint_32";

   procedure save_int_32 (buf : adalibpng.conf.png_bytep; i : adalibpng.conf.png_int_32)  -- /opt/homebrew/include/libpng16/png.h:2554
   with Import => True, 
        Convention => C, 
        External_Name => "png_save_int_32";

   procedure save_uint_16 (buf : adalibpng.conf.png_bytep; i : unsigned)  -- /opt/homebrew/include/libpng16/png.h:2562
   with Import => True, 
        Convention => C, 
        External_Name => "png_save_uint_16";

   procedure set_check_for_invalid_index (png_ptr : png_structrp; allowed : int)  -- /opt/homebrew/include/libpng16/png.h:2608
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_check_for_invalid_index";

   function get_palette_max (png_ptr : png_const_structp; info_ptr : png_const_infop) return int  -- /opt/homebrew/include/libpng16/png.h:2611
   with Import => True, 
        Convention => C, 
        External_Name => "png_get_palette_max";

   type png_control is null record;   -- incomplete struct

   type png_controlp is access all png_control;  -- /opt/homebrew/include/libpng16/png.h:2667

   subtype anon_array1921 is Interfaces.C.char_array (0 .. 63);
   type png_image is record
      opaque : png_controlp;  -- /opt/homebrew/include/libpng16/png.h:2670
      version : aliased adalibpng.conf.png_uint_32;  -- /opt/homebrew/include/libpng16/png.h:2671
      width : aliased adalibpng.conf.png_uint_32;  -- /opt/homebrew/include/libpng16/png.h:2672
      height : aliased adalibpng.conf.png_uint_32;  -- /opt/homebrew/include/libpng16/png.h:2673
      format : aliased adalibpng.conf.png_uint_32;  -- /opt/homebrew/include/libpng16/png.h:2674
      flags : aliased adalibpng.conf.png_uint_32;  -- /opt/homebrew/include/libpng16/png.h:2675
      colormap_entries : aliased adalibpng.conf.png_uint_32;  -- /opt/homebrew/include/libpng16/png.h:2676
      warning_or_error : aliased adalibpng.conf.png_uint_32;  -- /opt/homebrew/include/libpng16/png.h:2701
      message : aliased anon_array1921;  -- /opt/homebrew/include/libpng16/png.h:2703
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/libpng16/png.h:2704

   type png_imagep is access all png_image;  -- /opt/homebrew/include/libpng16/png.h:2704

   function image_begin_read_from_file (image : png_imagep; file_name : Interfaces.C.Strings.chars_ptr) return int  -- /opt/homebrew/include/libpng16/png.h:2976
   with Import => True, 
        Convention => C, 
        External_Name => "png_image_begin_read_from_file";

   function image_begin_read_from_stdio (image : png_imagep; the_file : access adalibpng.ICS.FILEs) return int  -- /opt/homebrew/include/libpng16/png.h:2982
   with Import => True, 
        Convention => C, 
        External_Name => "png_image_begin_read_from_stdio";

   function image_begin_read_from_memory
     (image : png_imagep;
      memory : adalibpng.conf.png_const_voidp;
      size : size_t) return int  -- /opt/homebrew/include/libpng16/png.h:2987
   with Import => True, 
        Convention => C, 
        External_Name => "png_image_begin_read_from_memory";

   function image_finish_read
     (image : png_imagep;
      background : png_const_colorp;
      buffer : System.Address;
      row_stride : adalibpng.conf.png_int_32;
      colormap : System.Address) return int  -- /opt/homebrew/include/libpng16/png.h:2991
   with Import => True, 
        Convention => C, 
        External_Name => "png_image_finish_read";

   procedure image_free (image : png_imagep)  -- /opt/homebrew/include/libpng16/png.h:3026
   with Import => True, 
        Convention => C, 
        External_Name => "png_image_free";

   function image_write_to_file
     (image : png_imagep;
      file : Interfaces.C.Strings.chars_ptr;
      convert_to_8bit : int;
      buffer : System.Address;
      row_stride : adalibpng.conf.png_int_32;
      colormap : System.Address) return int  -- /opt/homebrew/include/libpng16/png.h:3050
   with Import => True, 
        Convention => C, 
        External_Name => "png_image_write_to_file";

   function image_write_to_stdio
     (image : png_imagep;
      the_file : access adalibpng.ICS.FILEs ;
      convert_to_8_bit : int;
      buffer : System.Address;
      row_stride : adalibpng.conf.png_int_32;
      colormap : System.Address) return int  -- /opt/homebrew/include/libpng16/png.h:3055
   with Import => True, 
        Convention => C, 
        External_Name => "png_image_write_to_stdio";

   function image_write_to_memory
     (image : png_imagep;
      memory : System.Address;
      memory_bytes : access adalibpng.conf.png_alloc_size_t;
      convert_to_8_bit : int;
      buffer : System.Address;
      row_stride : adalibpng.conf.png_int_32;
      colormap : System.Address) return int  -- /opt/homebrew/include/libpng16/png.h:3082
   with Import => True, 
        Convention => C, 
        External_Name => "png_image_write_to_memory";

   function set_option
     (png_ptr : png_structrp;
      option : int;
      onoff : int) return int  -- /opt/homebrew/include/libpng16/png.h:3225
   with Import => True, 
        Convention => C, 
        External_Name => "png_set_option";

end png ;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
