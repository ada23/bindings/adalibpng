with Interfaces; use Interfaces;
with Interfaces.C; use Interfaces.C;
with Interfaces.C_Streams;
package Adalibpng is
    type time_t is new Unsigned_32 ;
    package ICS renames Interfaces.C_Streams;

    type tm is private ;
    type tmp is access all tm ;

    type timespec is private ;
    type timespecp is access all timespec ;

private

    type tm is record
      tm_sec : aliased int;  
      tm_min : aliased int;  
      tm_hour : aliased int;  
      tm_mday : aliased int;  
      tm_mon : aliased int;  
      tm_year : aliased int;  
      tm_wday : aliased int;  
      tm_yday : aliased int;  
      tm_isdst : aliased int;  
   end record
   with Convention => C_Pass_By_Copy;  

    type timespec is record
        tv_sec : aliased Unsigned_32 ;  
        tv_nsec : aliased Unsigned_32 ; 
    end record
    with Convention => C_Pass_By_Copy; 

end Adalibpng;
