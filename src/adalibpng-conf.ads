pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with System;
with Interfaces.C.Strings;

package adalibpng.conf is

   --  unsupported macro: PNG_CONST const
   --  arg-macro: procedure PNGARG (arglist)
   --    arglist
   --  unsupported macro: PNGCBAPI PNGCAPI
   --  unsupported macro: PNGAPI PNGCAPI
   --  arg-macro: procedure PNG_FUNCTION (type, name, args, attributes)
   --    attributes type name args
   --  arg-macro: procedure PNG_EXPORT_TYPE (type)
   --    PNG_IMPEXP type
   --  arg-macro: procedure PNG_EXPORTA (ordinal, type, name, args, attributes)
   --    PNG_FUNCTION(PNG_EXPORT_TYPE(type), (PNGAPI name), PNGARG(args), PNG_LINKAGE_API attributes)
   --  arg-macro: procedure PNG_EXPORT (ordinal, type, name, args)
   --    PNG_EXPORTA(ordinal, type, name, args, PNG_EMPTY)
   --  arg-macro: procedure PNG_CALLBACK (type, name, args)
   --    type (PNGCBAPI name) PNGARG(args)
   --  unsupported macro: PNG_USE_RESULT __attribute__((__warn_unused_result__))
   --  unsupported macro: PNG_NORETURN __attribute__((__noreturn__))
   --  unsupported macro: PNG_ALLOCATED __attribute__((__malloc__))
   --  unsupported macro: PNG_DEPRECATED __attribute__((__deprecated__))
   --  unsupported macro: PNG_PRIVATE __attribute__((__deprecated__))
   --  unsupported macro: PNG_RESTRICT __restrict
   subtype png_byte is unsigned_char;  -- /opt/homebrew/include/libpng16/pngconf.h:481

   subtype png_int_16 is short;  -- /opt/homebrew/include/libpng16/pngconf.h:489

   subtype png_uint_16 is unsigned_short;  -- /opt/homebrew/include/libpng16/pngconf.h:497

   subtype png_int_32 is int;  -- /opt/homebrew/include/libpng16/pngconf.h:503

   subtype png_uint_32 is unsigned;  -- /opt/homebrew/include/libpng16/pngconf.h:511

   subtype png_size_t is size_t;  -- /opt/homebrew/include/libpng16/pngconf.h:523

   subtype png_ptrdiff_t is ptrdiff_t;  -- /opt/homebrew/include/libpng16/pngconf.h:524

   subtype png_alloc_size_t is size_t;  -- /opt/homebrew/include/libpng16/pngconf.h:557

   subtype png_fixed_point is png_int_32;  -- /opt/homebrew/include/libpng16/pngconf.h:574

   type png_voidp is new System.Address;  -- /opt/homebrew/include/libpng16/pngconf.h:577

   type png_const_voidp is new System.Address;  -- /opt/homebrew/include/libpng16/pngconf.h:578

   type png_bytep is access all png_byte;  -- /opt/homebrew/include/libpng16/pngconf.h:579

   type png_const_bytep is access all png_byte;  -- /opt/homebrew/include/libpng16/pngconf.h:580

   type png_uint_32p is access all png_uint_32;  -- /opt/homebrew/include/libpng16/pngconf.h:581

   type png_const_uint_32p is access all png_uint_32;  -- /opt/homebrew/include/libpng16/pngconf.h:582

   type png_int_32p is access all png_int_32;  -- /opt/homebrew/include/libpng16/pngconf.h:583

   type png_const_int_32p is access all png_int_32;  -- /opt/homebrew/include/libpng16/pngconf.h:584

   type png_uint_16p is access all png_uint_16;  -- /opt/homebrew/include/libpng16/pngconf.h:585

   type png_const_uint_16p is access all png_uint_16;  -- /opt/homebrew/include/libpng16/pngconf.h:586

   type png_int_16p is access all png_int_16;  -- /opt/homebrew/include/libpng16/pngconf.h:587

   type png_const_int_16p is access all png_int_16;  -- /opt/homebrew/include/libpng16/pngconf.h:588

   type png_charp is new Interfaces.C.Strings.chars_ptr;  -- /opt/homebrew/include/libpng16/pngconf.h:589

   type png_const_charp is new Interfaces.C.Strings.chars_ptr;  -- /opt/homebrew/include/libpng16/pngconf.h:590
    type png_const_doublep is access all double ;
    
   type png_fixed_point_p is access all png_fixed_point;  -- /opt/homebrew/include/libpng16/pngconf.h:591

   type png_const_fixed_point_p is access all png_fixed_point;  -- /opt/homebrew/include/libpng16/pngconf.h:592

   type png_size_tp is access all size_t;  -- /opt/homebrew/include/libpng16/pngconf.h:593

   type png_const_size_tp is access all size_t;  -- /opt/homebrew/include/libpng16/pngconf.h:594

   type png_bytepp is new System.Address;  -- /opt/homebrew/include/libpng16/pngconf.h:606

   type png_uint_32pp is new System.Address;  -- /opt/homebrew/include/libpng16/pngconf.h:607

   type png_int_32pp is new System.Address;  -- /opt/homebrew/include/libpng16/pngconf.h:608

   type png_uint_16pp is new System.Address;  -- /opt/homebrew/include/libpng16/pngconf.h:609

   type png_int_16pp is new System.Address;  -- /opt/homebrew/include/libpng16/pngconf.h:610

   type png_const_charpp is new System.Address;  -- /opt/homebrew/include/libpng16/pngconf.h:611

   type png_charpp is new System.Address;  -- /opt/homebrew/include/libpng16/pngconf.h:612

   type png_fixed_point_pp is new System.Address;  -- /opt/homebrew/include/libpng16/pngconf.h:613

   type png_charppp is new System.Address;  -- /opt/homebrew/include/libpng16/pngconf.h:619

end adalibpng.conf ;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
